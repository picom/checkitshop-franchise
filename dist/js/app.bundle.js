(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
$(document).ready(function () {
	var responsiveModeTeblet = false;
	var responsiveModeFullHD = false;
	var anchorClick = false;
	var SLIDES_COUNT = 8;

	Modernizr.on('webp', function (result) {
		if (result) {
			$('.section').addClass('webp');
		} else {
			console.log('WebP is not supported!');
		}
	});

	if (window.matchMedia('(max-width: 768px)').matches) {
		responsiveModeTeblet = true;
	}

	if (window.matchMedia('(min-width: 1920px)').matches) {
		responsiveModeFullHD = true;
	}

	window.onresize = function () {
		if (window.matchMedia('(max-width: 768px)').matches) {
			responsiveModeTeblet = true;
		} else if (window.matchMedia('(min-width: 768px)').matches) {
			responsiveModeTeblet = false;
		}

		if (window.matchMedia('(min-width: 1920px)').matches) {
			responsiveModeFullHD = true;
		} else if (window.matchMedia('(max-width: 1920px)').matches) {
			responsiveModeFullHD = false;
		}
	};

	$('.subscribe-form__agree').click(function (event) {
		event.preventDefault();
		if ($('.subscribe-form__agree-checkbox').attr('checked')) {
			$('.subscribe-form__agree-checkbox').removeAttr('checked');
		} else {
			$('.subscribe-form__agree-checkbox').attr('checked', 'checked');
		}
	});

	function setAnchorActive(index) {
		for (i = SLIDES_COUNT; i >= index; i--) {
			$('[href="#slide' + i + '"]').removeClass('visited');
		}
		for (i = 1; i < index; i++) {
			$('[href="#slide' + i + '"]').addClass('visited');
		}
	}

	function animateListElements(element, delay, speed, margin, interval, direction) {
		element.children().delay(delay).each(function (index, value) {
			if (direction == 'left') {
				$(this).delay(interval * index).animate({
					opacity: '1',
					marginLeft: margin
				}, speed, 'swing');
			} else if (direction == 'right') {
				$(this).delay(interval * index).animate({
					opacity: '0',
					marginLeft: margin
				}, speed, 'swing');
			}
		});
	}

	function setNavSliderPosition() {
		$('.section').each(function (index, value) {
			if ($(this).hasClass('active')) {
				var i = 'anchor-' + (index + 1);
				$('.nav-active').css({
					left: document.getElementById(i).getBoundingClientRect().left + $('#' + i).width() / 2
				});
			}
		});
	}

	if (!responsiveModeTeblet) {
		$('#fullpage').fullpage({ //инициализируем fullpage.js
			scrollingSpeed: 1000,
			menu: '#menu',
			autoScrolling: true,
			lazyLoading: false,
			anchors: ['slide0', 'slide1', 'slide2', 'slide3', 'slide4', 'slide5', 'slide6', 'slide7'],
			easingcss3: 'ease-in-out',

			afterLoad: function (anchorLink, index) {
				var topMargin = '100';
				if (responsiveModeFullHD) {
					topMargin = '25%';
				} else {
					topMargin = '100';
				}

				//открытие слайда 1
				if (index == 1) {
					$('[href="#slide0"]').addClass('visited');
					for (i = SLIDES_COUNT; i >= index; i--) {
						$('[href="#slide' + i + '"]').removeClass('visited');
					}
					$('#section0 .slide__header').animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					$('#section0 .subscribe-form').delay(200).animate({
						top: topMargin,
						opacity: '1'
					}, 500);
					$('#section0 .border-bottom').delay(300).animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					setNavSliderPosition();
				}

				if (index == 2) {
					$('#section1 .slide__header').delay(500).animate({
						marginTop: '0',
						opacity: '1'
					}, 500);
					$('#section1 .border-bottom').delay(700).animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					$('#section1 .video-wrapper').delay(500).animate({
						top: '11%',
						opacity: '1'
					}, 500);
					$('[href="#slide1"]').addClass('visited');
					setNavSliderPosition();
					setAnchorActive(index);
				}

				if (index == 3) {
					$('#section2 .slide__header').delay(500).animate({
						marginTop: '0',
						opacity: '1'
					}, 500);
					$('#section2 .border-bottom').delay(700).animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					$('#section2 .slide__img-right').delay(700).animate({
						marginTop: '0',
						opacity: '1'
					}, 500);
					animateListElements($('#section2 .slide__header__list'), 800, 500, 0, 400, 'left');
					$('[href="#slide2"]').addClass('visited');
					setNavSliderPosition();
					setAnchorActive(index);
				}

				if (index == 4) {
					$('#section3 .slide__header').delay(500).animate({
						marginTop: '0',
						opacity: '1'
					}, 500);
					$('#section3 .border-bottom').delay(700).animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					$('#section3 .slide__img-right').delay(700).animate({
						marginTop: '0',
						opacity: '1'
					}, 500);
					animateListElements($('#section3 .slide__header__list'), 800, 500, 0, 400, 'left');
					$('[href="#slide3"]').addClass('visited');
					setNavSliderPosition();
					setAnchorActive(index);
				}

				if (index == 5) {
					$('#section4 .slide__header').delay(500).animate({
						marginTop: '0',
						opacity: '1'
					}, 500);
					$('#section4 .border-bottom').delay(700).animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					$('.slide__icons-wrapper').children().delay(700).each(function (index, value) {
						var self = $(this).not('.slide__icons-description');
						setTimeout(function () {
							self.css({
								opacity: '1',
								marginTop: '0'
							});
						}, index * 50);
					});
					$('[href="#slide4"]').addClass('visited');
					$('#section4 .slide__icons-description').each(function (index, value) {
						var marginLeft = $(this).prev().offset().left - $('.franchise-icon__circle').first().offset().left;
						$(this).css('margin-left', marginLeft);
					});
					setNavSliderPosition();
					setAnchorActive(index);
				}

				if (index == 6) {
					$('#section5 .slide__header').delay(500).animate({
						marginTop: '0',
						opacity: '1'
					}, 500);
					$('#section5 .border-bottom').delay(700).animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					$('#section5 .slide__description').delay(1000).animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					$('[href="#slide5"]').addClass('visited');
					setNavSliderPosition();
					setAnchorActive(index);
				}

				if (index == 7) {
					$('#section6_1 .slide__header').delay(500).animate({
						paddingTop: '0',
						opacity: '1'
					}, 500);
					$('#section6_1 .border-bottom').delay(700).animate({
						paddingLeft: '0',
						opacity: '1'
					}, 500);
					$('[href="#slide6"]').addClass('visited');
					setNavSliderPosition();
					setAnchorActive(index);
				}

				if (index == 8) {
					$('#section7 .slide__header h1').animate({
						marginLeft: '0'
					}, 500);
					$('#section7 .slide__header').animate({
						opacity: '1'
					}, 500);
					$('#section7 .subscribe-form').delay(200).animate({
						top: topMargin,
						opacity: '1'
					}, 500);
					$('#section7 .border-bottom').delay(300).animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					animateListElements($('#section7 .slide__header__list'), 800, 500, 0, 400, 'left');
					$('[href="#slide7"]').addClass('visited');
					setNavSliderPosition();
					setAnchorActive(index);
				}

				anchorClick = false;
			},

			onLeave: function (index, nextIndex, direction) {
				//переход с 1 на 2 слайдер
				if (index == 1 && direction == 'down') {
					$('#section0 .slide__header').delay(1000).animate({
						marginLeft: '400',
						opacity: '0'
					}, 50);
					$('#section0 .subscribe-form').delay(1200).animate({
						opacity: '0' }).delay(100).animate({ top: '400px' });
					$('#section0 .border-bottom').delay(1300).animate({
						marginLeft: '100',
						opacity: '0'
					}, 50);
					$('#section1 .slide__header').delay(500).animate({
						marginTop: '0',
						opacity: '1'
					}, 500);
					$('#section1 .border-bottom').delay(700).animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					$('#section1 .video-wrapper').show('fast', function () {}).delay(500).animate({
						top: '11%',
						opacity: '1'
					}, 300);
					$('[href="#slide1"]').addClass('visited');
					if (!anchorClick) {
						$('.nav-active').css({
							left: document.getElementById('anchor-' + (index + 1)).getBoundingClientRect().left + $('#anchor-' + (index + 1)).width() / 2
						});
					};
				}

				//переход со 2 на 1 слайдер
				else if (index == 2 && direction == 'up') {
						$('#section1 .slide__header').delay(1000).animate({
							marginTop: '400',
							opacity: '0'
						}, 50);
						$('#section1 .border-bottom').delay(1000).animate({
							marginLeft: '100',
							opacity: '0'
						}, 50);
						$('#section1 .video-wrapper').delay(1000).animate({
							top: '90%',
							opacity: '0'
						}, 50);
						$('[href="#slide1"]').removeClass('visited');
						if (!anchorClick) {
							$('.nav-active').css({
								left: document.getElementById('anchor-' + (index - 1)).getBoundingClientRect().left + $('#anchor-' + (index - 1)).width() / 2
							});
						};
					}

					//переход со 2 на 3 слайдер
					else if (index == 2 && direction == 'down') {
							$('#section1 .slide__header').delay(1000).animate({
								marginTop: '400',
								opacity: '0'
							}, 50);
							$('#section1 .border-bottom').delay(1000).animate({
								marginLeft: '100',
								opacity: '0'
							}, 50);
							$('#section1 .video-wrapper').delay(1000).hide('fast', function () {}).animate({
								top: '90%',
								opacity: '0'
							});
							$('#section2 .slide__header').delay(500).animate({
								marginTop: '0',
								opacity: '1'
							}, 50);
							$('#section2 .border-bottom').delay(700).animate({
								marginLeft: '0',
								opacity: '1'
							}, 500);
							$('#section2 .slide__img-right').delay(700).animate({
								marginTop: '0',
								opacity: '1'
							}, 500);
							animateListElements($('#section2 .slide__header__list'), 800, 500, 0, 400, 'left');
							$('[href="#slide2"]').addClass('visited');
							if (!anchorClick) {
								$('.nav-active').css({
									left: document.getElementById('anchor-' + (index + 1)).getBoundingClientRect().left + $('#anchor-' + (index + 1)).width() / 2
								});
							};
						}

						//переход с 3 на 2 слайдер
						else if (index == 3 && direction == 'up') {
								$('#section1 .slide__header').delay(500).animate({
									marginTop: '0',
									opacity: '1'
								}, 500);
								$('#section1 .border-bottom').delay(700).animate({
									marginLeft: '0',
									opacity: '1'
								}, 500);
								$('#section1 .video-wrapper').delay(500).show('fast', function () {}).animate({
									top: '11%',
									opacity: '1'
								}, 300);
								$('#section2 .slide__header').delay(500).animate({
									marginTop: '400',
									opacity: '0'
								}, 50);
								$('#section2 .border-bottom').delay(700).animate({
									marginLeft: '100',
									opacity: '0'
								}, 50);
								$('#section2 .slide__img-right').delay(700).animate({
									marginTop: '400',
									opacity: '0'
								}, 50);
								animateListElements($('#section2 .slide__header__list'), 950, 10, 300, 0, 'right');
								$('[href="#slide2"]').removeClass('visited');
								if (!anchorClick) {
									$('.nav-active').css({
										left: document.getElementById('anchor-' + (index - 1)).getBoundingClientRect().left + $('#anchor-' + (index - 1)).width() / 2
									});
								};
							}

							//переход с 3 на 4 слайдер
							else if (index == 3 && direction == 'down') {
									$('#section2 .slide__header').delay(500).animate({
										marginTop: '400',
										opacity: '0'
									}, 50);
									$('#section2 .border-bottom').delay(700).animate({
										marginLeft: '100',
										opacity: '0'
									}, 50);
									$('#section2 .slide__img-right').delay(700).animate({
										marginTop: '400px',
										opacity: '0'
									}, 50);
									$('#section2 .slide__header__list li').delay(1000).animate({
										marginLeft: '300',
										opacity: '0'
									}, 10);
									$('#section3 .slide__header').delay(500).animate({
										marginTop: '0',
										opacity: '1'
									}, 500);
									$('#section3 .border-bottom').delay(700).animate({
										marginLeft: '0',
										opacity: '1'
									}, 500);
									$('#section3 .slide__img-right').delay(700).animate({
										marginTop: '0',
										opacity: '1'
									}, 500);
									animateListElements($('#section3 .slide__header__list'), 800, 500, 0, 400, 'left');
									$('[href="#slide3"]').addClass('visited');
									if (!anchorClick) {
										$('.nav-active').css({
											left: document.getElementById('anchor-' + (index + 1)).getBoundingClientRect().left + $('#anchor-' + (index + 1)).width() / 2
										});
									};
								}

								//переход с 4 на 3 слайдер
								else if (index == 4 && direction == 'up') {
										$('#section3 .slide__header').delay(500).animate({
											marginTop: '400',
											opacity: '0'
										}, 50);
										$('#section3 .border-bottom').delay(700).animate({
											marginLeft: '100',
											opacity: '0'
										}, 50);
										$('#section3 .slide__img-right').delay(700).animate({
											marginTop: '400',
											opacity: '0'
										}, 50);
										animateListElements($('#section3 .slide__header__list'), 950, 10, 300, 0, 'right');
										$('#section2 .slide__header').delay(500).animate({
											marginTop: '0',
											opacity: '1'
										}, 500);
										$('#section2 .border-bottom').delay(700).animate({
											marginLeft: '0',
											opacity: '1'
										}, 500);
										$('#section2 .slide__img-right').delay(700).animate({
											marginTop: '0',
											opacity: '1'
										}, 500);
										animateListElements($('#section2 .slide__header__list'), 800, 500, 0, 400, 'left');
										$('[href="#slide3"]').removeClass('visited');
										if (!anchorClick) {
											$('.nav-active').css({
												left: document.getElementById('anchor-' + (index - 1)).getBoundingClientRect().left + $('#anchor-' + (index - 1)).width() / 2
											});
										};
									}

									//переход с 4 на 5 слайдер
									else if (index == 4 && direction == 'down') {
											$('#section3 .slide__header').delay(500).animate({
												marginTop: '400',
												opacity: '0'
											}, 50);
											$('#section3 .border-bottom').delay(700).animate({
												marginLeft: '100',
												opacity: '0'
											}, 50);
											$('#section3 .slide__img-right').delay(700).animate({
												marginTop: '120',
												opacity: '0'
											}, 50);
											$('#section3 .slide__header__list li').delay(500).animate({
												marginLeft: '320',
												opacity: '0'
											}, 500);
											$('#section4 .slide__header').delay(500).animate({
												marginTop: '0',
												opacity: '1'
											}, 500);
											$('#section4 .border-bottom').delay(700).animate({
												marginLeft: '0',
												opacity: '1'
											}, 500);
											$('#section4 .slide__icons-description').each(function (index, value) {
												var marginLeft = $(this).prev().offset().left - $('.franchise-icon__circle').first().offset().left;
												$(this).css('margin-left', marginLeft);
											});
											$('.slide__icons-wrapper').children().each(function (index, value) {
												setTimeout(function () {
													var self = $(this).not('.slide__icons-description');
													setTimeout(function () {
														self.css({
															opacity: '1',
															marginTop: '0'
														});
													}, index * 50);
												}, 600);
											});
											$('[href="#slide4"]').addClass('visited');
											if (!anchorClick) {
												$('.nav-active').css({
													left: document.getElementById('anchor-' + (index + 1)).getBoundingClientRect().left + $('#anchor-' + (index + 1)).width() / 2
												});
											};
										}

										//переход с 5 на 4 слайдер
										else if (index == 5 && direction == 'up') {
												$('#section4 .slide__header').delay(500).animate({
													marginTop: '400',
													opacity: '0'
												}, 50);
												$('#section4 .border-bottom').delay(700).animate({
													marginLeft: '100',
													opacity: '0'
												}, 50);
												$('#section4 .franchise-icon__circle').delay(1000).animate({
													marginTop: '300',
													opacity: '0'
												}, 50);
												$('#section4 .franchise-icon-dot').delay(1000).animate({
													marginTop: '300',
													opacity: '0'
												}, 50);
												$('#section4 .slide__icons-description').delay(700).animate({
													marginLeft: '400',
													opacity: '0'
												}, 50);
												$('#section3 .slide__header').delay(500).animate({
													marginTop: '0',
													opacity: '1'
												}, 500);
												$('#section3 .border-bottom').delay(700).animate({
													marginLeft: '0',
													opacity: '1'
												}, 500);
												animateListElements($('#section3 .slide__header__list'), 800, 500, 0, 400, 'left');
												$('#section3 .slide__img-right').delay(700).animate({
													marginTop: '0',
													opacity: '1'
												}, 500);

												$('[href="#slide4"]').removeClass('visited');
												if (!anchorClick) {
													$('.nav-active').css({
														left: document.getElementById('anchor-' + (index - 1)).getBoundingClientRect().left + $('#anchor-' + (index - 1)).width() / 2
													});
												};
											}

											//переход с 5 на 6 слайдер
											else if (index == 5 && direction == 'down') {
													$('#section4 .slide__header').delay(1000).animate({
														marginTop: '400',
														opacity: '0'
													}, 50);
													$('#section4 .border-bottom').delay(1000).animate({
														marginLeft: '100',
														opacity: '0'
													}, 50);
													$('#section4 .franchise-icon__circle').delay(1000).animate({
														marginTop: '300',
														opacity: '0'
													}, 50);
													$('#section4 .franchise-icon-dot').delay(1000).animate({
														marginTop: '300',
														opacity: '0'
													}, 50);
													$('#section4 .slide__icons-description').delay(700).animate({
														marginLeft: '400',
														opacity: '0'
													}, 50);
													$('#section5 .slide__header').delay(500).animate({
														marginTop: '0',
														opacity: '1'
													}, 500);
													$('#section5 .border-bottom').delay(700).animate({
														marginLeft: '0',
														opacity: '1'
													}, 500);
													$('#section5 .slide__description').delay(1000).animate({
														marginLeft: '0',
														opacity: '1'
													}, 500);
													$('[href="#slide5"]').addClass('visited');
													if (!anchorClick) {
														$('.nav-active').css({
															left: document.getElementById('anchor-' + (index + 1)).getBoundingClientRect().left + $('#anchor-' + (index + 1)).width() / 2
														});
													};
												}

												//переход с 6 на 5 слайдер
												else if (index == 6 && direction == 'up') {
														$('#section5 .slide__header').delay(500).animate({
															marginTop: '400',
															opacity: '0'
														}, 50);
														$('#section5 .border-bottom').delay(700).animate({
															marginLeft: '100',
															opacity: '0'
														}, 50);
														$('#section5 .slide__description').delay(1000).animate({
															marginLeft: '400',
															opacity: '0'
														}, 500);
														$('#section4 .slide__header').delay(500).animate({
															marginTop: '0',
															opacity: '1'
														}, 500);
														$('#section4 .border-bottom').delay(700).animate({
															marginLeft: '0',
															opacity: '1'
														}, 500);
														$('#section4 .slide__icons-description').each(function (index, value) {
															var marginLeft = $(this).prev().offset().left - $('.franchise-icon__circle').first().offset().left;
															$(this).css('margin-left', marginLeft);
														});
														setTimeout(function () {
															var self = $(this).not('.slide__icons-description');
															setTimeout(function () {
																self.css({
																	opacity: '1',
																	marginTop: '0'
																});
															}, index * 50);
														}, 600);
														$('[href="#slide5"]').removeClass('visited');
														if (!anchorClick) {
															$('.nav-active').css({
																left: document.getElementById('anchor-' + (index - 1)).getBoundingClientRect().left + $('#anchor-' + (index - 1)).width() / 2
															});
														};
														$.fn.fullpage.setAllowScrolling(true, 'up, down');
													}

													//переход с 6 на 7 слайдер
													else if (index == 6 && direction == 'down') {
															$('#section5 .slide__header').delay(1000).animate({
																marginTop: '400',
																opacity: '0'
															}, 50);
															$('#section5 .border-bottom').delay(1000).animate({
																marginLeft: '100',
																opacity: '0'
															}, 50);
															$('#section5 .slide__description').delay(1000).animate({
																marginLeft: '400',
																opacity: '0'
															}, 50);
															$('#section6_1 .slide__header').delay(500).animate({
																paddingTop: '0',
																opacity: '1'
															}, 500);
															$('#section6_1 .border-bottom').delay(700).animate({
																paddingLeft: '0',
																opacity: '1'
															}, 500);
															$('[href="#slide6"]').addClass('visited');
															if (!anchorClick) {
																$('.nav-active').css({
																	left: document.getElementById('anchor-' + (index + 1)).getBoundingClientRect().left + $('#anchor-' + (index + 1)).width() / 2
																});
															};

															$.fn.fullpage.setAllowScrolling(false, 'down'); //зпрещаем юзеру крутить слайдер вниз
														}

														//переход с 7 на 6 слайдер
														else if (index == 7 && direction == 'up') {
																$('#section6_1 .slide__header').delay(500).animate({
																	paddingTop: '400',
																	opacity: '0'
																}, 50);
																$('#section6_1 .border-bottom').delay(700).animate({
																	paddingLeft: '400',
																	opacity: '0'
																}, 50);
																$('#section5 .slide__header').delay(500).animate({
																	marginTop: '0',
																	opacity: '1'
																}, 500);
																$('#section5 .border-bottom').delay(700).animate({
																	marginLeft: '0',
																	opacity: '1'
																}, 500);
																$('#section5 .slide__description').delay(1000).animate({
																	marginLeft: '0',
																	opacity: '1'
																}, 500);
																$('[href="#slide5"]').removeClass('visited');
																if (!anchorClick) {
																	$('.nav-active').css({
																		left: document.getElementById('anchor-' + (index - 1)).getBoundingClientRect().left + $('#anchor-' + (index - 1)).width() / 2
																	});
																};
																$.fn.fullpage.setAllowScrolling(true, 'up, down');
															}

															//переход с 8 на 7 слайдер
															else if (index == 8 && direction == 'up') {
																	$('#section7 .slide__header').delay(1000).animate({
																		opacity: '0'
																	}, 50);
																	$('#section7 .slide__header h1').delay(1000).animate({
																		marginLeft: '400'
																	}, 50);
																	$('#section7 .border-bottom').delay(1000).animate({
																		marginLeft: '400',
																		opacity: '0'
																	}, 50);
																	$('#section7 .slide__header__list li').delay(1000).animate({
																		marginLeft: '400',
																		opacity: '0'
																	}, 50);
																	$('#section7 .subscribe-form').delay(1000).animate({
																		top: '400',
																		opacity: '0'
																	}, 50);
																	$('#section6_1 .slide__header').delay(500).animate({
																		paddingTop: '0',
																		opacity: '1'
																	}, 500);
																	$('#section6_1 .border-bottom').delay(700).animate({
																		paddingLeft: '0',
																		opacity: '1'
																	}, 500);
																	$('[href="#slide6"]').addClass('visited');
																	if (!anchorClick) {
																		$('.nav-active').css({
																			left: document.getElementById('anchor-' + (index - 1)).getBoundingClientRect().left + $('#anchor-' + (index - 1)).width() / 2
																		});
																	};
																}

				anchorClick = false;
			}
		});

		document.getElementById("section6").addEventListener('wheel', function (e) {
			//отслеживаем колесо мыши на горизонтальном слайдере
			if (e.deltaY < 0) {
				//прокуртка вверх
				document.getElementById("section6_1").addEventListener('wheel', function (e) {
					if (e.deltaY < 0) {
						$.fn.fullpage.moveTo('slide5');
						return;
					}
				});
				$.fn.fullpage.moveSlideLeft();
			}
			if (e.deltaY > 0) {
				//прокуртка вниз
				document.getElementById("section6_4").addEventListener('wheel', function (e) {
					if (e.deltaY > 0) {
						$.fn.fullpage.moveTo('slide7');
						return;
					}
				});
				$.fn.fullpage.moveSlideRight();
			}
		});

		$('#menu a').click(function (event) {
			var id = $(this).attr('id');
			anchorClick = true;
			$('.nav-active').css({
				left: document.getElementById(id).getBoundingClientRect().left + $('#' + id).width() / 2
			});
		});
	}

	//задаем атрибуты элементов страницы для адаптива
	if (responsiveModeTeblet) {
		$('.m-menu-list a').removeClass('visited');
		$('.franchise-icon__circle.active').removeClass('active');
		$('.franchise-icon__circle.inactive').removeClass('inactive');
	} else if (!responsiveModeTeblet) {
		$('.m-menu').css('display', 'none');
	}

	//обработка кликов по иконкам страницы "франшизы"
	$('.franchise-icon__circle').click(function () {
		event.preventDefault();
		if ($(this).hasClass('inactive')) {
			$('.franchise-icon__circle.active').addClass('inactive');
			$('.franchise-icon__circle.active').removeClass('active');
			$(this).addClass('active');
			$(this).removeClass('inactive');

			$(".slide__icons-description.active").addClass("inactive").removeClass('active');

			if ($(this).next().hasClass('inactive')) {
				$(this).next().removeClass('inactive').addClass('active');
			}
		}
	});

	//обработка кликов меню в адаптиве
	$('.m-menu-button').click(function () {
		event.preventDefault();
		$(this).toggleClass('burger');
		$(this).toggleClass('close');
		$('.m-menu').slideToggle('fast', function () {});
	});

	$('.m-menu-list a').click(function () {
		event.preventDefault();
		$('.m-menu-list a').removeClass('visited');
		$('.m-menu-button').toggleClass('burger');
		$('.m-menu-button').toggleClass('close');
		$(this).addClass('visited');
		$('.m-menu').slideToggle('fast', function () {});
		$('html, body').animate({
			scrollTop: $($(this).attr('href')).offset().top - $("header").height()
		}, 1000);
	});
});

},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvanMvYXBwLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FDQUEsRUFBRSxRQUFGLEVBQVksS0FBWixDQUFrQixZQUFVO0FBQzNCLEtBQUksdUJBQXVCLEtBQTNCO0FBQ0EsS0FBSSx1QkFBdUIsS0FBM0I7QUFDQSxLQUFJLGNBQWMsS0FBbEI7QUFDQSxLQUFJLGVBQWUsQ0FBbkI7O0FBRUEsV0FBVSxFQUFWLENBQWEsTUFBYixFQUFxQixVQUFTLE1BQVQsRUFBaUI7QUFDcEMsTUFBSSxNQUFKLEVBQVk7QUFDVixLQUFFLFVBQUYsRUFBYyxRQUFkLENBQXVCLE1BQXZCO0FBQ0QsR0FGRCxNQUVPO0FBQ0wsV0FBUSxHQUFSLENBQVksd0JBQVo7QUFDRDtBQUNGLEVBTkQ7O0FBUUEsS0FBSSxPQUFPLFVBQVAsQ0FBa0Isb0JBQWxCLEVBQXdDLE9BQTVDLEVBQXFEO0FBQ3BELHlCQUF1QixJQUF2QjtBQUNBOztBQUVELEtBQUksT0FBTyxVQUFQLENBQWtCLHFCQUFsQixFQUF5QyxPQUE3QyxFQUFzRDtBQUNyRCx5QkFBdUIsSUFBdkI7QUFDQTs7QUFFRCxRQUFPLFFBQVAsR0FBa0IsWUFBVztBQUM1QixNQUFJLE9BQU8sVUFBUCxDQUFrQixvQkFBbEIsRUFBd0MsT0FBNUMsRUFBcUQ7QUFDcEQsMEJBQXVCLElBQXZCO0FBQ0EsR0FGRCxNQUlLLElBQUksT0FBTyxVQUFQLENBQWtCLG9CQUFsQixFQUF3QyxPQUE1QyxFQUFxRDtBQUN6RCwwQkFBdUIsS0FBdkI7QUFDQTs7QUFFRCxNQUFJLE9BQU8sVUFBUCxDQUFrQixxQkFBbEIsRUFBeUMsT0FBN0MsRUFBc0Q7QUFDckQsMEJBQXVCLElBQXZCO0FBQ0EsR0FGRCxNQUlLLElBQUksT0FBTyxVQUFQLENBQWtCLHFCQUFsQixFQUF5QyxPQUE3QyxFQUFzRDtBQUMxRCwwQkFBdUIsS0FBdkI7QUFDQTtBQUNELEVBaEJEOztBQWtCQSxHQUFFLHdCQUFGLEVBQTRCLEtBQTVCLENBQWtDLFVBQVMsS0FBVCxFQUFnQjtBQUNqRCxRQUFNLGNBQU47QUFDQSxNQUFJLEVBQUUsaUNBQUYsRUFBcUMsSUFBckMsQ0FBMEMsU0FBMUMsQ0FBSixFQUEwRDtBQUNuRCxLQUFFLGlDQUFGLEVBQXFDLFVBQXJDLENBQWdELFNBQWhEO0FBQ0gsR0FGSixNQUVVO0FBQ0gsS0FBRSxpQ0FBRixFQUFxQyxJQUFyQyxDQUEwQyxTQUExQyxFQUFxRCxTQUFyRDtBQUNIO0FBQ0osRUFQRDs7QUFTQSxVQUFTLGVBQVQsQ0FBeUIsS0FBekIsRUFBZ0M7QUFDL0IsT0FBSyxJQUFJLFlBQVQsRUFBdUIsS0FBRyxLQUExQixFQUFpQyxHQUFqQyxFQUFzQztBQUNyQyxLQUFFLGtCQUFrQixDQUFsQixHQUFzQixJQUF4QixFQUE4QixXQUE5QixDQUEwQyxTQUExQztBQUNBO0FBQ0QsT0FBSyxJQUFJLENBQVQsRUFBWSxJQUFFLEtBQWQsRUFBcUIsR0FBckIsRUFBMEI7QUFDekIsS0FBRSxrQkFBa0IsQ0FBbEIsR0FBc0IsSUFBeEIsRUFBOEIsUUFBOUIsQ0FBdUMsU0FBdkM7QUFDQTtBQUNEOztBQUVELFVBQVMsbUJBQVQsQ0FBNkIsT0FBN0IsRUFBc0MsS0FBdEMsRUFBNkMsS0FBN0MsRUFBb0QsTUFBcEQsRUFBNEQsUUFBNUQsRUFBc0UsU0FBdEUsRUFBaUY7QUFDaEYsVUFBUSxRQUFSLEdBQW1CLEtBQW5CLENBQXlCLEtBQXpCLEVBQWdDLElBQWhDLENBQXFDLFVBQVMsS0FBVCxFQUFnQixLQUFoQixFQUFzQjtBQUMxRCxPQUFJLGFBQWEsTUFBakIsRUFBeUI7QUFDeEIsTUFBRSxJQUFGLEVBQVEsS0FBUixDQUFjLFdBQVcsS0FBekIsRUFBZ0MsT0FBaEMsQ0FBd0M7QUFDM0IsY0FBUyxHQURrQjtBQUUzQixpQkFBWTtBQUZlLEtBQXhDLEVBR1ksS0FIWixFQUdtQixPQUhuQjtBQUlBLElBTEQsTUFNSyxJQUFJLGFBQWEsT0FBakIsRUFBMEI7QUFDOUIsTUFBRSxJQUFGLEVBQVEsS0FBUixDQUFjLFdBQVcsS0FBekIsRUFBZ0MsT0FBaEMsQ0FBd0M7QUFDM0IsY0FBUyxHQURrQjtBQUUzQixpQkFBWTtBQUZlLEtBQXhDLEVBR1ksS0FIWixFQUdtQixPQUhuQjtBQUlBO0FBQ0ssR0FiUDtBQWNBOztBQUVELFVBQVMsb0JBQVQsR0FBZ0M7QUFDL0IsSUFBRSxVQUFGLEVBQWMsSUFBZCxDQUFtQixVQUFTLEtBQVQsRUFBZ0IsS0FBaEIsRUFBc0I7QUFDeEMsT0FBSSxFQUFFLElBQUYsRUFBUSxRQUFSLENBQWlCLFFBQWpCLENBQUosRUFBZ0M7QUFDL0IsUUFBSSxJQUFJLGFBQVcsUUFBTSxDQUFqQixDQUFSO0FBQ0EsTUFBRSxhQUFGLEVBQWlCLEdBQWpCLENBQXFCO0FBQ3BCLFdBQU0sU0FBUyxjQUFULENBQXdCLENBQXhCLEVBQTJCLHFCQUEzQixHQUFtRCxJQUFuRCxHQUEwRCxFQUFFLE1BQUksQ0FBTixFQUFTLEtBQVQsS0FBaUI7QUFEN0QsS0FBckI7QUFHQTtBQUNELEdBUEQ7QUFRQTs7QUFFRCxLQUFJLENBQUMsb0JBQUwsRUFBMkI7QUFDMUIsSUFBRSxXQUFGLEVBQWUsUUFBZixDQUF3QixFQUFFO0FBQ3pCLG1CQUFnQixJQURPO0FBRXZCLFNBQU0sT0FGaUI7QUFHdkIsa0JBQWUsSUFIUTtBQUl2QixnQkFBYSxLQUpVO0FBS3ZCLFlBQVMsQ0FBQyxRQUFELEVBQVcsUUFBWCxFQUFxQixRQUFyQixFQUErQixRQUEvQixFQUF5QyxRQUF6QyxFQUFtRCxRQUFuRCxFQUE2RCxRQUE3RCxFQUF1RSxRQUF2RSxDQUxjO0FBTXZCLGVBQVksYUFOVzs7QUFRdkIsY0FBVyxVQUFTLFVBQVQsRUFBcUIsS0FBckIsRUFBMkI7QUFDckMsUUFBSSxZQUFVLEtBQWQ7QUFDQSxRQUFJLG9CQUFKLEVBQTBCO0FBQ3pCLGlCQUFVLEtBQVY7QUFDQSxLQUZELE1BR0s7QUFDSixpQkFBVSxLQUFWO0FBQ0E7O0FBRUQ7QUFDQSxRQUFHLFNBQVMsQ0FBWixFQUFjO0FBQ2IsT0FBRSxrQkFBRixFQUFzQixRQUF0QixDQUErQixTQUEvQjtBQUNBLFVBQUssSUFBSSxZQUFULEVBQXVCLEtBQUcsS0FBMUIsRUFBaUMsR0FBakMsRUFBc0M7QUFDckMsUUFBRSxrQkFBa0IsQ0FBbEIsR0FBc0IsSUFBeEIsRUFBOEIsV0FBOUIsQ0FBMEMsU0FBMUM7QUFDQTtBQUNELE9BQUUsMEJBQUYsRUFBOEIsT0FBOUIsQ0FBc0M7QUFDckMsa0JBQVksR0FEeUI7QUFFckMsZUFBUztBQUY0QixNQUF0QyxFQUdHLEdBSEg7QUFJQSxPQUFFLDJCQUFGLEVBQStCLEtBQS9CLENBQXFDLEdBQXJDLEVBQTBDLE9BQTFDLENBQWtEO0FBQ2pELFdBQUssU0FENEM7QUFFakQsZUFBUztBQUZ3QyxNQUFsRCxFQUdHLEdBSEg7QUFJQSxPQUFFLDBCQUFGLEVBQThCLEtBQTlCLENBQW9DLEdBQXBDLEVBQXlDLE9BQXpDLENBQWlEO0FBQ2hELGtCQUFZLEdBRG9DO0FBRWhELGVBQVM7QUFGdUMsTUFBakQsRUFHRyxHQUhIO0FBSUE7QUFDQTs7QUFFRCxRQUFHLFNBQVMsQ0FBWixFQUFjO0FBQ2IsT0FBRSwwQkFBRixFQUE4QixLQUE5QixDQUFvQyxHQUFwQyxFQUF5QyxPQUF6QyxDQUFpRDtBQUNoRCxpQkFBVyxHQURxQztBQUVoRCxlQUFTO0FBRnVDLE1BQWpELEVBR0csR0FISDtBQUlBLE9BQUUsMEJBQUYsRUFBOEIsS0FBOUIsQ0FBb0MsR0FBcEMsRUFBeUMsT0FBekMsQ0FBaUQ7QUFDaEQsa0JBQVksR0FEb0M7QUFFaEQsZUFBUztBQUZ1QyxNQUFqRCxFQUdHLEdBSEg7QUFJQSxPQUFFLDBCQUFGLEVBQThCLEtBQTlCLENBQW9DLEdBQXBDLEVBQXlDLE9BQXpDLENBQWlEO0FBQ2hELFdBQUssS0FEMkM7QUFFaEQsZUFBUztBQUZ1QyxNQUFqRCxFQUdHLEdBSEg7QUFJQSxPQUFFLGtCQUFGLEVBQXNCLFFBQXRCLENBQStCLFNBQS9CO0FBQ0E7QUFDQSxxQkFBZ0IsS0FBaEI7QUFDQTs7QUFFRCxRQUFHLFNBQVMsQ0FBWixFQUFjO0FBQ2IsT0FBRSwwQkFBRixFQUE4QixLQUE5QixDQUFvQyxHQUFwQyxFQUF5QyxPQUF6QyxDQUFpRDtBQUNoRCxpQkFBVyxHQURxQztBQUVoRCxlQUFTO0FBRnVDLE1BQWpELEVBR0csR0FISDtBQUlBLE9BQUUsMEJBQUYsRUFBOEIsS0FBOUIsQ0FBb0MsR0FBcEMsRUFBeUMsT0FBekMsQ0FBaUQ7QUFDaEQsa0JBQVksR0FEb0M7QUFFaEQsZUFBUztBQUZ1QyxNQUFqRCxFQUdHLEdBSEg7QUFJQSxPQUFFLDZCQUFGLEVBQWlDLEtBQWpDLENBQXVDLEdBQXZDLEVBQTRDLE9BQTVDLENBQW9EO0FBQ25ELGlCQUFXLEdBRHdDO0FBRW5ELGVBQVM7QUFGMEMsTUFBcEQsRUFHRyxHQUhIO0FBSUEseUJBQW9CLEVBQUUsZ0NBQUYsQ0FBcEIsRUFBeUQsR0FBekQsRUFBOEQsR0FBOUQsRUFBbUUsQ0FBbkUsRUFBc0UsR0FBdEUsRUFBMkUsTUFBM0U7QUFDQSxPQUFFLGtCQUFGLEVBQXNCLFFBQXRCLENBQStCLFNBQS9CO0FBQ0E7QUFDQSxxQkFBZ0IsS0FBaEI7QUFDQTs7QUFFRCxRQUFHLFNBQVMsQ0FBWixFQUFjO0FBQ2IsT0FBRSwwQkFBRixFQUE4QixLQUE5QixDQUFvQyxHQUFwQyxFQUF5QyxPQUF6QyxDQUFpRDtBQUNoRCxpQkFBVyxHQURxQztBQUVoRCxlQUFTO0FBRnVDLE1BQWpELEVBR0csR0FISDtBQUlBLE9BQUUsMEJBQUYsRUFBOEIsS0FBOUIsQ0FBb0MsR0FBcEMsRUFBeUMsT0FBekMsQ0FBaUQ7QUFDaEQsa0JBQVksR0FEb0M7QUFFaEQsZUFBUztBQUZ1QyxNQUFqRCxFQUdHLEdBSEg7QUFJQSxPQUFFLDZCQUFGLEVBQWlDLEtBQWpDLENBQXVDLEdBQXZDLEVBQTRDLE9BQTVDLENBQW9EO0FBQ25ELGlCQUFXLEdBRHdDO0FBRW5ELGVBQVM7QUFGMEMsTUFBcEQsRUFHRyxHQUhIO0FBSUEseUJBQW9CLEVBQUUsZ0NBQUYsQ0FBcEIsRUFBeUQsR0FBekQsRUFBOEQsR0FBOUQsRUFBbUUsQ0FBbkUsRUFBc0UsR0FBdEUsRUFBMkUsTUFBM0U7QUFDQSxPQUFFLGtCQUFGLEVBQXNCLFFBQXRCLENBQStCLFNBQS9CO0FBQ0E7QUFDQSxxQkFBZ0IsS0FBaEI7QUFDQTs7QUFFRCxRQUFHLFNBQVMsQ0FBWixFQUFjO0FBQ2IsT0FBRSwwQkFBRixFQUE4QixLQUE5QixDQUFvQyxHQUFwQyxFQUF5QyxPQUF6QyxDQUFpRDtBQUNoRCxpQkFBVyxHQURxQztBQUVoRCxlQUFTO0FBRnVDLE1BQWpELEVBR0csR0FISDtBQUlBLE9BQUUsMEJBQUYsRUFBOEIsS0FBOUIsQ0FBb0MsR0FBcEMsRUFBeUMsT0FBekMsQ0FBaUQ7QUFDaEQsa0JBQVksR0FEb0M7QUFFaEQsZUFBUztBQUZ1QyxNQUFqRCxFQUdHLEdBSEg7QUFJQSxPQUFFLHVCQUFGLEVBQTJCLFFBQTNCLEdBQXNDLEtBQXRDLENBQTRDLEdBQTVDLEVBQWlELElBQWpELENBQXNELFVBQVMsS0FBVCxFQUFnQixLQUFoQixFQUFzQjtBQUMzRSxVQUFJLE9BQU8sRUFBRSxJQUFGLEVBQVEsR0FBUixDQUFZLDJCQUFaLENBQVg7QUFDWSxpQkFBVyxZQUFZO0FBQzlCLFlBQUssR0FBTCxDQUFTO0FBQ0EsaUJBQVMsR0FEVDtBQUVBLG1CQUFXO0FBRlgsUUFBVDtBQUlELE9BTFEsRUFLTixRQUFNLEVBTEE7QUFNUixNQVJMO0FBU0EsT0FBRSxrQkFBRixFQUFzQixRQUF0QixDQUErQixTQUEvQjtBQUNBLE9BQUUscUNBQUYsRUFBeUMsSUFBekMsQ0FBOEMsVUFBUyxLQUFULEVBQWdCLEtBQWhCLEVBQXVCO0FBQ3BFLFVBQUksYUFBYSxFQUFFLElBQUYsRUFBUSxJQUFSLEdBQWUsTUFBZixHQUF3QixJQUF4QixHQUErQixFQUFFLHlCQUFGLEVBQTZCLEtBQTdCLEdBQXFDLE1BQXJDLEdBQThDLElBQTlGO0FBQ0MsUUFBRSxJQUFGLEVBQVEsR0FBUixDQUFZLGFBQVosRUFBMkIsVUFBM0I7QUFDRCxNQUhEO0FBSUE7QUFDQSxxQkFBZ0IsS0FBaEI7QUFDQTs7QUFFRCxRQUFHLFNBQVMsQ0FBWixFQUFjO0FBQ2IsT0FBRSwwQkFBRixFQUE4QixLQUE5QixDQUFvQyxHQUFwQyxFQUF5QyxPQUF6QyxDQUFpRDtBQUNoRCxpQkFBVyxHQURxQztBQUVoRCxlQUFTO0FBRnVDLE1BQWpELEVBR0csR0FISDtBQUlBLE9BQUUsMEJBQUYsRUFBOEIsS0FBOUIsQ0FBb0MsR0FBcEMsRUFBeUMsT0FBekMsQ0FBaUQ7QUFDaEQsa0JBQVksR0FEb0M7QUFFaEQsZUFBUztBQUZ1QyxNQUFqRCxFQUdHLEdBSEg7QUFJQSxPQUFFLCtCQUFGLEVBQW1DLEtBQW5DLENBQXlDLElBQXpDLEVBQStDLE9BQS9DLENBQXVEO0FBQ3RELGtCQUFZLEdBRDBDO0FBRXRELGVBQVM7QUFGNkMsTUFBdkQsRUFHRyxHQUhIO0FBSUEsT0FBRSxrQkFBRixFQUFzQixRQUF0QixDQUErQixTQUEvQjtBQUNBO0FBQ0EscUJBQWdCLEtBQWhCO0FBQ0E7O0FBRUQsUUFBRyxTQUFTLENBQVosRUFBYztBQUNiLE9BQUUsNEJBQUYsRUFBZ0MsS0FBaEMsQ0FBc0MsR0FBdEMsRUFBMkMsT0FBM0MsQ0FBbUQ7QUFDbEQsa0JBQVksR0FEc0M7QUFFbEQsZUFBUztBQUZ5QyxNQUFuRCxFQUdHLEdBSEg7QUFJQSxPQUFFLDRCQUFGLEVBQWdDLEtBQWhDLENBQXNDLEdBQXRDLEVBQTJDLE9BQTNDLENBQW1EO0FBQ2xELG1CQUFhLEdBRHFDO0FBRWxELGVBQVM7QUFGeUMsTUFBbkQsRUFHRyxHQUhIO0FBSUEsT0FBRSxrQkFBRixFQUFzQixRQUF0QixDQUErQixTQUEvQjtBQUNBO0FBQ0EscUJBQWdCLEtBQWhCO0FBQ0E7O0FBRUQsUUFBRyxTQUFTLENBQVosRUFBYztBQUNiLE9BQUUsNkJBQUYsRUFBaUMsT0FBakMsQ0FBeUM7QUFDeEMsa0JBQVk7QUFENEIsTUFBekMsRUFFRyxHQUZIO0FBR0EsT0FBRSwwQkFBRixFQUE4QixPQUE5QixDQUFzQztBQUNyQyxlQUFTO0FBRDRCLE1BQXRDLEVBRUcsR0FGSDtBQUdBLE9BQUUsMkJBQUYsRUFBK0IsS0FBL0IsQ0FBcUMsR0FBckMsRUFBMEMsT0FBMUMsQ0FBa0Q7QUFDakQsV0FBSyxTQUQ0QztBQUVqRCxlQUFTO0FBRndDLE1BQWxELEVBR0csR0FISDtBQUlBLE9BQUUsMEJBQUYsRUFBOEIsS0FBOUIsQ0FBb0MsR0FBcEMsRUFBeUMsT0FBekMsQ0FBaUQ7QUFDaEQsa0JBQVksR0FEb0M7QUFFaEQsZUFBUztBQUZ1QyxNQUFqRCxFQUdHLEdBSEg7QUFJQSx5QkFBb0IsRUFBRSxnQ0FBRixDQUFwQixFQUF5RCxHQUF6RCxFQUE4RCxHQUE5RCxFQUFtRSxDQUFuRSxFQUFzRSxHQUF0RSxFQUEyRSxNQUEzRTtBQUNBLE9BQUUsa0JBQUYsRUFBc0IsUUFBdEIsQ0FBK0IsU0FBL0I7QUFDQTtBQUNBLHFCQUFnQixLQUFoQjtBQUNBOztBQUVELGtCQUFjLEtBQWQ7QUFDQSxJQS9Lc0I7O0FBaUx2QixZQUFTLFVBQVMsS0FBVCxFQUFnQixTQUFoQixFQUEyQixTQUEzQixFQUFxQztBQUM3QztBQUNBLFFBQUcsU0FBUyxDQUFULElBQWMsYUFBWSxNQUE3QixFQUFvQztBQUNuQyxPQUFFLDBCQUFGLEVBQThCLEtBQTlCLENBQW9DLElBQXBDLEVBQTBDLE9BQTFDLENBQWtEO0FBQ2pELGtCQUFZLEtBRHFDO0FBRWpELGVBQVM7QUFGd0MsTUFBbEQsRUFHRyxFQUhIO0FBSUEsT0FBRSwyQkFBRixFQUErQixLQUEvQixDQUFxQyxJQUFyQyxFQUEyQyxPQUEzQyxDQUFtRDtBQUNsRCxlQUFTLEdBRHlDLEVBQW5ELEVBQ2dCLEtBRGhCLENBQ3NCLEdBRHRCLEVBQzJCLE9BRDNCLENBQ21DLEVBQUMsS0FBSyxPQUFOLEVBRG5DO0FBRUEsT0FBRSwwQkFBRixFQUE4QixLQUE5QixDQUFvQyxJQUFwQyxFQUEwQyxPQUExQyxDQUFrRDtBQUNqRCxrQkFBWSxLQURxQztBQUVqRCxlQUFTO0FBRndDLE1BQWxELEVBR0csRUFISDtBQUlBLE9BQUUsMEJBQUYsRUFBOEIsS0FBOUIsQ0FBb0MsR0FBcEMsRUFBeUMsT0FBekMsQ0FBaUQ7QUFDaEQsaUJBQVcsR0FEcUM7QUFFaEQsZUFBUztBQUZ1QyxNQUFqRCxFQUdHLEdBSEg7QUFJQSxPQUFFLDBCQUFGLEVBQThCLEtBQTlCLENBQW9DLEdBQXBDLEVBQXlDLE9BQXpDLENBQWlEO0FBQ2hELGtCQUFZLEdBRG9DO0FBRWhELGVBQVM7QUFGdUMsTUFBakQsRUFHRyxHQUhIO0FBSUEsT0FBRSwwQkFBRixFQUE4QixJQUE5QixDQUFtQyxNQUFuQyxFQUEyQyxZQUFVLENBQUUsQ0FBdkQsRUFBeUQsS0FBekQsQ0FBK0QsR0FBL0QsRUFBb0UsT0FBcEUsQ0FBNEU7QUFDM0UsV0FBSyxLQURzRTtBQUUzRSxlQUFTO0FBRmtFLE1BQTVFLEVBR0csR0FISDtBQUlBLE9BQUUsa0JBQUYsRUFBc0IsUUFBdEIsQ0FBK0IsU0FBL0I7QUFDQSxTQUFJLENBQUMsV0FBTCxFQUFrQjtBQUNqQixRQUFFLGFBQUYsRUFBaUIsR0FBakIsQ0FBcUI7QUFDcEIsYUFBTSxTQUFTLGNBQVQsQ0FBd0IsYUFBYSxRQUFNLENBQW5CLENBQXhCLEVBQStDLHFCQUEvQyxHQUF1RSxJQUF2RSxHQUE4RSxFQUFFLGNBQWMsUUFBTSxDQUFwQixDQUFGLEVBQTBCLEtBQTFCLEtBQWtDO0FBRGxHLE9BQXJCO0FBR0E7QUFDRDs7QUFFRDtBQS9CQSxTQWdDSyxJQUFHLFNBQVMsQ0FBVCxJQUFjLGFBQVksSUFBN0IsRUFBa0M7QUFDdEMsUUFBRSwwQkFBRixFQUE4QixLQUE5QixDQUFvQyxJQUFwQyxFQUEwQyxPQUExQyxDQUFrRDtBQUNqRCxrQkFBVyxLQURzQztBQUVqRCxnQkFBUztBQUZ3QyxPQUFsRCxFQUdHLEVBSEg7QUFJQSxRQUFFLDBCQUFGLEVBQThCLEtBQTlCLENBQW9DLElBQXBDLEVBQTBDLE9BQTFDLENBQWtEO0FBQ2pELG1CQUFZLEtBRHFDO0FBRWpELGdCQUFTO0FBRndDLE9BQWxELEVBR0csRUFISDtBQUlBLFFBQUUsMEJBQUYsRUFBOEIsS0FBOUIsQ0FBb0MsSUFBcEMsRUFBMEMsT0FBMUMsQ0FBa0Q7QUFDakQsWUFBSyxLQUQ0QztBQUVqRCxnQkFBUztBQUZ3QyxPQUFsRCxFQUdHLEVBSEg7QUFJQSxRQUFFLGtCQUFGLEVBQXNCLFdBQXRCLENBQWtDLFNBQWxDO0FBQ0EsVUFBSSxDQUFDLFdBQUwsRUFBa0I7QUFDakIsU0FBRSxhQUFGLEVBQWlCLEdBQWpCLENBQXFCO0FBQ3BCLGNBQU0sU0FBUyxjQUFULENBQXdCLGFBQWEsUUFBTSxDQUFuQixDQUF4QixFQUErQyxxQkFBL0MsR0FBdUUsSUFBdkUsR0FBOEUsRUFBRSxjQUFjLFFBQU0sQ0FBcEIsQ0FBRixFQUEwQixLQUExQixLQUFrQztBQURsRyxRQUFyQjtBQUdBO0FBQ0Q7O0FBRUQ7QUFyQkssVUFzQkEsSUFBRyxTQUFTLENBQVQsSUFBYyxhQUFZLE1BQTdCLEVBQW9DO0FBQ3hDLFNBQUUsMEJBQUYsRUFBOEIsS0FBOUIsQ0FBb0MsSUFBcEMsRUFBMEMsT0FBMUMsQ0FBa0Q7QUFDakQsbUJBQVcsS0FEc0M7QUFFakQsaUJBQVM7QUFGd0MsUUFBbEQsRUFHRyxFQUhIO0FBSUEsU0FBRSwwQkFBRixFQUE4QixLQUE5QixDQUFvQyxJQUFwQyxFQUEwQyxPQUExQyxDQUFrRDtBQUNqRCxvQkFBWSxLQURxQztBQUVqRCxpQkFBUztBQUZ3QyxRQUFsRCxFQUdHLEVBSEg7QUFJQSxTQUFFLDBCQUFGLEVBQThCLEtBQTlCLENBQW9DLElBQXBDLEVBQTBDLElBQTFDLENBQStDLE1BQS9DLEVBQXVELFlBQVcsQ0FBRSxDQUFwRSxFQUFzRSxPQUF0RSxDQUE4RTtBQUM3RSxhQUFLLEtBRHdFO0FBRTdFLGlCQUFTO0FBRm9FLFFBQTlFO0FBSUEsU0FBRSwwQkFBRixFQUE4QixLQUE5QixDQUFvQyxHQUFwQyxFQUF5QyxPQUF6QyxDQUFpRDtBQUNoRCxtQkFBVyxHQURxQztBQUVoRCxpQkFBUztBQUZ1QyxRQUFqRCxFQUdHLEVBSEg7QUFJQSxTQUFFLDBCQUFGLEVBQThCLEtBQTlCLENBQW9DLEdBQXBDLEVBQXlDLE9BQXpDLENBQWlEO0FBQ2hELG9CQUFZLEdBRG9DO0FBRWhELGlCQUFTO0FBRnVDLFFBQWpELEVBR0csR0FISDtBQUlBLFNBQUUsNkJBQUYsRUFBaUMsS0FBakMsQ0FBdUMsR0FBdkMsRUFBNEMsT0FBNUMsQ0FBb0Q7QUFDbkQsbUJBQVcsR0FEd0M7QUFFbkQsaUJBQVM7QUFGMEMsUUFBcEQsRUFHRyxHQUhIO0FBSUEsMkJBQW9CLEVBQUUsZ0NBQUYsQ0FBcEIsRUFBeUQsR0FBekQsRUFBOEQsR0FBOUQsRUFBbUUsQ0FBbkUsRUFBc0UsR0FBdEUsRUFBMkUsTUFBM0U7QUFDQSxTQUFFLGtCQUFGLEVBQXNCLFFBQXRCLENBQStCLFNBQS9CO0FBQ0EsV0FBSSxDQUFDLFdBQUwsRUFBa0I7QUFDakIsVUFBRSxhQUFGLEVBQWlCLEdBQWpCLENBQXFCO0FBQ3BCLGVBQU0sU0FBUyxjQUFULENBQXdCLGFBQWEsUUFBTSxDQUFuQixDQUF4QixFQUErQyxxQkFBL0MsR0FBdUUsSUFBdkUsR0FBOEUsRUFBRSxjQUFjLFFBQU0sQ0FBcEIsQ0FBRixFQUEwQixLQUExQixLQUFrQztBQURsRyxTQUFyQjtBQUdBO0FBQ0Q7O0FBRUQ7QUFsQ0ssV0FtQ0EsSUFBRyxTQUFTLENBQVQsSUFBYyxhQUFZLElBQTdCLEVBQWtDO0FBQ3RDLFVBQUUsMEJBQUYsRUFBOEIsS0FBOUIsQ0FBb0MsR0FBcEMsRUFBeUMsT0FBekMsQ0FBaUQ7QUFDaEQsb0JBQVcsR0FEcUM7QUFFaEQsa0JBQVM7QUFGdUMsU0FBakQsRUFHRyxHQUhIO0FBSUEsVUFBRSwwQkFBRixFQUE4QixLQUE5QixDQUFvQyxHQUFwQyxFQUF5QyxPQUF6QyxDQUFpRDtBQUNoRCxxQkFBWSxHQURvQztBQUVoRCxrQkFBUztBQUZ1QyxTQUFqRCxFQUdHLEdBSEg7QUFJQSxVQUFFLDBCQUFGLEVBQThCLEtBQTlCLENBQW9DLEdBQXBDLEVBQXlDLElBQXpDLENBQThDLE1BQTlDLEVBQXNELFlBQVUsQ0FBRSxDQUFsRSxFQUFvRSxPQUFwRSxDQUE0RTtBQUMzRSxjQUFLLEtBRHNFO0FBRTNFLGtCQUFTO0FBRmtFLFNBQTVFLEVBR0csR0FISDtBQUlBLFVBQUUsMEJBQUYsRUFBOEIsS0FBOUIsQ0FBb0MsR0FBcEMsRUFBeUMsT0FBekMsQ0FBaUQ7QUFDaEQsb0JBQVcsS0FEcUM7QUFFaEQsa0JBQVM7QUFGdUMsU0FBakQsRUFHRyxFQUhIO0FBSUEsVUFBRSwwQkFBRixFQUE4QixLQUE5QixDQUFvQyxHQUFwQyxFQUF5QyxPQUF6QyxDQUFpRDtBQUNoRCxxQkFBWSxLQURvQztBQUVoRCxrQkFBUztBQUZ1QyxTQUFqRCxFQUdHLEVBSEg7QUFJQSxVQUFFLDZCQUFGLEVBQWlDLEtBQWpDLENBQXVDLEdBQXZDLEVBQTRDLE9BQTVDLENBQW9EO0FBQ25ELG9CQUFXLEtBRHdDO0FBRW5ELGtCQUFTO0FBRjBDLFNBQXBELEVBR0csRUFISDtBQUlBLDRCQUFvQixFQUFFLGdDQUFGLENBQXBCLEVBQXlELEdBQXpELEVBQThELEVBQTlELEVBQWtFLEdBQWxFLEVBQXVFLENBQXZFLEVBQTBFLE9BQTFFO0FBQ0EsVUFBRSxrQkFBRixFQUFzQixXQUF0QixDQUFrQyxTQUFsQztBQUNBLFlBQUksQ0FBQyxXQUFMLEVBQWtCO0FBQ2pCLFdBQUUsYUFBRixFQUFpQixHQUFqQixDQUFxQjtBQUNwQixnQkFBTSxTQUFTLGNBQVQsQ0FBd0IsYUFBYSxRQUFNLENBQW5CLENBQXhCLEVBQStDLHFCQUEvQyxHQUF1RSxJQUF2RSxHQUE4RSxFQUFFLGNBQWMsUUFBTSxDQUFwQixDQUFGLEVBQTBCLEtBQTFCLEtBQWtDO0FBRGxHLFVBQXJCO0FBR0E7QUFDRDs7QUFFRDtBQWxDSyxZQW1DQSxJQUFHLFNBQVMsQ0FBVCxJQUFjLGFBQVksTUFBN0IsRUFBb0M7QUFDeEMsV0FBRSwwQkFBRixFQUE4QixLQUE5QixDQUFvQyxHQUFwQyxFQUF5QyxPQUF6QyxDQUFpRDtBQUNoRCxxQkFBVyxLQURxQztBQUVoRCxtQkFBUztBQUZ1QyxVQUFqRCxFQUdHLEVBSEg7QUFJQSxXQUFFLDBCQUFGLEVBQThCLEtBQTlCLENBQW9DLEdBQXBDLEVBQXlDLE9BQXpDLENBQWlEO0FBQ2hELHNCQUFZLEtBRG9DO0FBRWhELG1CQUFTO0FBRnVDLFVBQWpELEVBR0csRUFISDtBQUlBLFdBQUUsNkJBQUYsRUFBaUMsS0FBakMsQ0FBdUMsR0FBdkMsRUFBNEMsT0FBNUMsQ0FBb0Q7QUFDbkQscUJBQVcsT0FEd0M7QUFFbkQsbUJBQVM7QUFGMEMsVUFBcEQsRUFHRyxFQUhIO0FBSUEsV0FBRSxtQ0FBRixFQUF1QyxLQUF2QyxDQUE2QyxJQUE3QyxFQUFtRCxPQUFuRCxDQUEyRDtBQUMxRCxzQkFBWSxLQUQ4QztBQUUxRCxtQkFBUztBQUZpRCxVQUEzRCxFQUdHLEVBSEg7QUFJQSxXQUFFLDBCQUFGLEVBQThCLEtBQTlCLENBQW9DLEdBQXBDLEVBQXlDLE9BQXpDLENBQWlEO0FBQ2hELHFCQUFXLEdBRHFDO0FBRWhELG1CQUFTO0FBRnVDLFVBQWpELEVBR0csR0FISDtBQUlBLFdBQUUsMEJBQUYsRUFBOEIsS0FBOUIsQ0FBb0MsR0FBcEMsRUFBeUMsT0FBekMsQ0FBaUQ7QUFDaEQsc0JBQVksR0FEb0M7QUFFaEQsbUJBQVM7QUFGdUMsVUFBakQsRUFHRyxHQUhIO0FBSUEsV0FBRSw2QkFBRixFQUFpQyxLQUFqQyxDQUF1QyxHQUF2QyxFQUE0QyxPQUE1QyxDQUFvRDtBQUNuRCxxQkFBVyxHQUR3QztBQUVuRCxtQkFBUztBQUYwQyxVQUFwRCxFQUdHLEdBSEg7QUFJQSw2QkFBb0IsRUFBRSxnQ0FBRixDQUFwQixFQUF5RCxHQUF6RCxFQUE4RCxHQUE5RCxFQUFtRSxDQUFuRSxFQUFzRSxHQUF0RSxFQUEyRSxNQUEzRTtBQUNBLFdBQUUsa0JBQUYsRUFBc0IsUUFBdEIsQ0FBK0IsU0FBL0I7QUFDQSxhQUFJLENBQUMsV0FBTCxFQUFrQjtBQUNqQixZQUFFLGFBQUYsRUFBaUIsR0FBakIsQ0FBcUI7QUFDcEIsaUJBQU0sU0FBUyxjQUFULENBQXdCLGFBQWEsUUFBTSxDQUFuQixDQUF4QixFQUErQyxxQkFBL0MsR0FBdUUsSUFBdkUsR0FBOEUsRUFBRSxjQUFjLFFBQU0sQ0FBcEIsQ0FBRixFQUEwQixLQUExQixLQUFrQztBQURsRyxXQUFyQjtBQUdBO0FBQ0Q7O0FBRUQ7QUF0Q0ssYUF1Q0EsSUFBRyxTQUFTLENBQVQsSUFBYyxhQUFZLElBQTdCLEVBQWtDO0FBQ3RDLFlBQUUsMEJBQUYsRUFBOEIsS0FBOUIsQ0FBb0MsR0FBcEMsRUFBeUMsT0FBekMsQ0FBaUQ7QUFDaEQsc0JBQVcsS0FEcUM7QUFFaEQsb0JBQVM7QUFGdUMsV0FBakQsRUFHRyxFQUhIO0FBSUEsWUFBRSwwQkFBRixFQUE4QixLQUE5QixDQUFvQyxHQUFwQyxFQUF5QyxPQUF6QyxDQUFpRDtBQUNoRCx1QkFBWSxLQURvQztBQUVoRCxvQkFBUztBQUZ1QyxXQUFqRCxFQUdHLEVBSEg7QUFJQSxZQUFFLDZCQUFGLEVBQWlDLEtBQWpDLENBQXVDLEdBQXZDLEVBQTRDLE9BQTVDLENBQW9EO0FBQ25ELHNCQUFXLEtBRHdDO0FBRW5ELG9CQUFTO0FBRjBDLFdBQXBELEVBR0csRUFISDtBQUlBLDhCQUFvQixFQUFFLGdDQUFGLENBQXBCLEVBQXlELEdBQXpELEVBQThELEVBQTlELEVBQWtFLEdBQWxFLEVBQXVFLENBQXZFLEVBQTBFLE9BQTFFO0FBQ0EsWUFBRSwwQkFBRixFQUE4QixLQUE5QixDQUFvQyxHQUFwQyxFQUF5QyxPQUF6QyxDQUFpRDtBQUNoRCxzQkFBVyxHQURxQztBQUVoRCxvQkFBUztBQUZ1QyxXQUFqRCxFQUdHLEdBSEg7QUFJQSxZQUFFLDBCQUFGLEVBQThCLEtBQTlCLENBQW9DLEdBQXBDLEVBQXlDLE9BQXpDLENBQWlEO0FBQ2hELHVCQUFZLEdBRG9DO0FBRWhELG9CQUFTO0FBRnVDLFdBQWpELEVBR0csR0FISDtBQUlBLFlBQUUsNkJBQUYsRUFBaUMsS0FBakMsQ0FBdUMsR0FBdkMsRUFBNEMsT0FBNUMsQ0FBb0Q7QUFDbkQsc0JBQVcsR0FEd0M7QUFFbkQsb0JBQVM7QUFGMEMsV0FBcEQsRUFHRyxHQUhIO0FBSUEsOEJBQW9CLEVBQUUsZ0NBQUYsQ0FBcEIsRUFBeUQsR0FBekQsRUFBOEQsR0FBOUQsRUFBbUUsQ0FBbkUsRUFBc0UsR0FBdEUsRUFBMkUsTUFBM0U7QUFDQSxZQUFFLGtCQUFGLEVBQXNCLFdBQXRCLENBQWtDLFNBQWxDO0FBQ0EsY0FBSSxDQUFDLFdBQUwsRUFBa0I7QUFDakIsYUFBRSxhQUFGLEVBQWlCLEdBQWpCLENBQXFCO0FBQ3BCLGtCQUFNLFNBQVMsY0FBVCxDQUF3QixhQUFhLFFBQU0sQ0FBbkIsQ0FBeEIsRUFBK0MscUJBQS9DLEdBQXVFLElBQXZFLEdBQThFLEVBQUUsY0FBYyxRQUFNLENBQXBCLENBQUYsRUFBMEIsS0FBMUIsS0FBa0M7QUFEbEcsWUFBckI7QUFHQTtBQUNEOztBQUVEO0FBbkNLLGNBb0NBLElBQUcsU0FBUyxDQUFULElBQWMsYUFBWSxNQUE3QixFQUFvQztBQUN4QyxhQUFFLDBCQUFGLEVBQThCLEtBQTlCLENBQW9DLEdBQXBDLEVBQXlDLE9BQXpDLENBQWlEO0FBQ2hELHVCQUFXLEtBRHFDO0FBRWhELHFCQUFTO0FBRnVDLFlBQWpELEVBR0csRUFISDtBQUlBLGFBQUUsMEJBQUYsRUFBOEIsS0FBOUIsQ0FBb0MsR0FBcEMsRUFBeUMsT0FBekMsQ0FBaUQ7QUFDaEQsd0JBQVksS0FEb0M7QUFFaEQscUJBQVM7QUFGdUMsWUFBakQsRUFHRyxFQUhIO0FBSUEsYUFBRSw2QkFBRixFQUFpQyxLQUFqQyxDQUF1QyxHQUF2QyxFQUE0QyxPQUE1QyxDQUFvRDtBQUNuRCx1QkFBVyxLQUR3QztBQUVuRCxxQkFBUztBQUYwQyxZQUFwRCxFQUdHLEVBSEg7QUFJQSxhQUFFLG1DQUFGLEVBQXVDLEtBQXZDLENBQTZDLEdBQTdDLEVBQWtELE9BQWxELENBQTBEO0FBQ3pELHdCQUFZLEtBRDZDO0FBRXpELHFCQUFTO0FBRmdELFlBQTFELEVBR0csR0FISDtBQUlBLGFBQUUsMEJBQUYsRUFBOEIsS0FBOUIsQ0FBb0MsR0FBcEMsRUFBeUMsT0FBekMsQ0FBaUQ7QUFDaEQsdUJBQVcsR0FEcUM7QUFFaEQscUJBQVM7QUFGdUMsWUFBakQsRUFHRyxHQUhIO0FBSUEsYUFBRSwwQkFBRixFQUE4QixLQUE5QixDQUFvQyxHQUFwQyxFQUF5QyxPQUF6QyxDQUFpRDtBQUNoRCx3QkFBWSxHQURvQztBQUVoRCxxQkFBUztBQUZ1QyxZQUFqRCxFQUdHLEdBSEg7QUFJQSxhQUFFLHFDQUFGLEVBQXlDLElBQXpDLENBQThDLFVBQVMsS0FBVCxFQUFnQixLQUFoQixFQUF1QjtBQUNwRSxnQkFBSSxhQUFhLEVBQUUsSUFBRixFQUFRLElBQVIsR0FBZSxNQUFmLEdBQXdCLElBQXhCLEdBQStCLEVBQUUseUJBQUYsRUFBNkIsS0FBN0IsR0FBcUMsTUFBckMsR0FBOEMsSUFBOUY7QUFDQyxjQUFFLElBQUYsRUFBUSxHQUFSLENBQVksYUFBWixFQUEyQixVQUEzQjtBQUNELFlBSEQ7QUFJQSxhQUFFLHVCQUFGLEVBQTJCLFFBQTNCLEdBQXNDLElBQXRDLENBQTJDLFVBQVMsS0FBVCxFQUFnQixLQUFoQixFQUFzQjtBQUNqRSx1QkFBVyxZQUFXO0FBQ3JCLGlCQUFJLE9BQU8sRUFBRSxJQUFGLEVBQVEsR0FBUixDQUFZLDJCQUFaLENBQVg7QUFDWSx3QkFBVyxZQUFXO0FBQzdCLG1CQUFLLEdBQUwsQ0FBUztBQUNBLHdCQUFTLEdBRFQ7QUFFQSwwQkFBVztBQUZYLGVBQVQ7QUFJRCxjQUxRLEVBS04sUUFBTSxFQUxBO0FBTUgsYUFSVixFQVFZLEdBUlo7QUFTSSxZQVZKO0FBV0EsYUFBRSxrQkFBRixFQUFzQixRQUF0QixDQUErQixTQUEvQjtBQUNBLGVBQUksQ0FBQyxXQUFMLEVBQWtCO0FBQ2pCLGNBQUUsYUFBRixFQUFpQixHQUFqQixDQUFxQjtBQUNwQixtQkFBTSxTQUFTLGNBQVQsQ0FBd0IsYUFBYSxRQUFNLENBQW5CLENBQXhCLEVBQStDLHFCQUEvQyxHQUF1RSxJQUF2RSxHQUE4RSxFQUFFLGNBQWMsUUFBTSxDQUFwQixDQUFGLEVBQTBCLEtBQTFCLEtBQWtDO0FBRGxHLGFBQXJCO0FBR0E7QUFDRDs7QUFFRDtBQWhESyxlQWlEQSxJQUFHLFNBQVMsQ0FBVCxJQUFjLGFBQVksSUFBN0IsRUFBa0M7QUFDdEMsY0FBRSwwQkFBRixFQUE4QixLQUE5QixDQUFvQyxHQUFwQyxFQUF5QyxPQUF6QyxDQUFpRDtBQUNoRCx3QkFBVyxLQURxQztBQUVoRCxzQkFBUztBQUZ1QyxhQUFqRCxFQUdHLEVBSEg7QUFJQSxjQUFFLDBCQUFGLEVBQThCLEtBQTlCLENBQW9DLEdBQXBDLEVBQXlDLE9BQXpDLENBQWlEO0FBQ2hELHlCQUFZLEtBRG9DO0FBRWhELHNCQUFTO0FBRnVDLGFBQWpELEVBR0csRUFISDtBQUlBLGNBQUUsbUNBQUYsRUFBdUMsS0FBdkMsQ0FBNkMsSUFBN0MsRUFBbUQsT0FBbkQsQ0FBMkQ7QUFDMUQsd0JBQVcsS0FEK0M7QUFFMUQsc0JBQVM7QUFGaUQsYUFBM0QsRUFHRyxFQUhIO0FBSUEsY0FBRSwrQkFBRixFQUFtQyxLQUFuQyxDQUF5QyxJQUF6QyxFQUErQyxPQUEvQyxDQUF1RDtBQUN0RCx3QkFBVyxLQUQyQztBQUV0RCxzQkFBUztBQUY2QyxhQUF2RCxFQUdHLEVBSEg7QUFJQSxjQUFFLHFDQUFGLEVBQXlDLEtBQXpDLENBQStDLEdBQS9DLEVBQW9ELE9BQXBELENBQTREO0FBQzNELHlCQUFZLEtBRCtDO0FBRTNELHNCQUFTO0FBRmtELGFBQTVELEVBR0csRUFISDtBQUlBLGNBQUUsMEJBQUYsRUFBOEIsS0FBOUIsQ0FBb0MsR0FBcEMsRUFBeUMsT0FBekMsQ0FBaUQ7QUFDaEQsd0JBQVcsR0FEcUM7QUFFaEQsc0JBQVM7QUFGdUMsYUFBakQsRUFHRyxHQUhIO0FBSUEsY0FBRSwwQkFBRixFQUE4QixLQUE5QixDQUFvQyxHQUFwQyxFQUF5QyxPQUF6QyxDQUFpRDtBQUNoRCx5QkFBWSxHQURvQztBQUVoRCxzQkFBUztBQUZ1QyxhQUFqRCxFQUdHLEdBSEg7QUFJQSxnQ0FBb0IsRUFBRSxnQ0FBRixDQUFwQixFQUF5RCxHQUF6RCxFQUE4RCxHQUE5RCxFQUFtRSxDQUFuRSxFQUFzRSxHQUF0RSxFQUEyRSxNQUEzRTtBQUNBLGNBQUUsNkJBQUYsRUFBaUMsS0FBakMsQ0FBdUMsR0FBdkMsRUFBNEMsT0FBNUMsQ0FBb0Q7QUFDbkQsd0JBQVcsR0FEd0M7QUFFbkQsc0JBQVM7QUFGMEMsYUFBcEQsRUFHRyxHQUhIOztBQUtBLGNBQUUsa0JBQUYsRUFBc0IsV0FBdEIsQ0FBa0MsU0FBbEM7QUFDQSxnQkFBSSxDQUFDLFdBQUwsRUFBa0I7QUFDakIsZUFBRSxhQUFGLEVBQWlCLEdBQWpCLENBQXFCO0FBQ3BCLG9CQUFNLFNBQVMsY0FBVCxDQUF3QixhQUFhLFFBQU0sQ0FBbkIsQ0FBeEIsRUFBK0MscUJBQS9DLEdBQXVFLElBQXZFLEdBQThFLEVBQUUsY0FBYyxRQUFNLENBQXBCLENBQUYsRUFBMEIsS0FBMUIsS0FBa0M7QUFEbEcsY0FBckI7QUFHQTtBQUNEOztBQUVEO0FBM0NLLGdCQTRDQSxJQUFHLFNBQVMsQ0FBVCxJQUFjLGFBQVksTUFBN0IsRUFBb0M7QUFDeEMsZUFBRSwwQkFBRixFQUE4QixLQUE5QixDQUFvQyxJQUFwQyxFQUEwQyxPQUExQyxDQUFrRDtBQUNqRCx5QkFBVyxLQURzQztBQUVqRCx1QkFBUztBQUZ3QyxjQUFsRCxFQUdHLEVBSEg7QUFJQSxlQUFFLDBCQUFGLEVBQThCLEtBQTlCLENBQW9DLElBQXBDLEVBQTBDLE9BQTFDLENBQWtEO0FBQ2pELDBCQUFZLEtBRHFDO0FBRWpELHVCQUFTO0FBRndDLGNBQWxELEVBR0csRUFISDtBQUlBLGVBQUUsbUNBQUYsRUFBdUMsS0FBdkMsQ0FBNkMsSUFBN0MsRUFBbUQsT0FBbkQsQ0FBMkQ7QUFDMUQseUJBQVcsS0FEK0M7QUFFMUQsdUJBQVM7QUFGaUQsY0FBM0QsRUFHRyxFQUhIO0FBSUEsZUFBRSwrQkFBRixFQUFtQyxLQUFuQyxDQUF5QyxJQUF6QyxFQUErQyxPQUEvQyxDQUF1RDtBQUN0RCx5QkFBVyxLQUQyQztBQUV0RCx1QkFBUztBQUY2QyxjQUF2RCxFQUdHLEVBSEg7QUFJQSxlQUFFLHFDQUFGLEVBQXlDLEtBQXpDLENBQStDLEdBQS9DLEVBQW9ELE9BQXBELENBQTREO0FBQzNELDBCQUFZLEtBRCtDO0FBRTNELHVCQUFTO0FBRmtELGNBQTVELEVBR0csRUFISDtBQUlBLGVBQUUsMEJBQUYsRUFBOEIsS0FBOUIsQ0FBb0MsR0FBcEMsRUFBeUMsT0FBekMsQ0FBaUQ7QUFDaEQseUJBQVcsR0FEcUM7QUFFaEQsdUJBQVM7QUFGdUMsY0FBakQsRUFHRyxHQUhIO0FBSUEsZUFBRSwwQkFBRixFQUE4QixLQUE5QixDQUFvQyxHQUFwQyxFQUF5QyxPQUF6QyxDQUFpRDtBQUNoRCwwQkFBWSxHQURvQztBQUVoRCx1QkFBUztBQUZ1QyxjQUFqRCxFQUdHLEdBSEg7QUFJQSxlQUFFLCtCQUFGLEVBQW1DLEtBQW5DLENBQXlDLElBQXpDLEVBQStDLE9BQS9DLENBQXVEO0FBQ3RELDBCQUFZLEdBRDBDO0FBRXRELHVCQUFTO0FBRjZDLGNBQXZELEVBR0csR0FISDtBQUlBLGVBQUUsa0JBQUYsRUFBc0IsUUFBdEIsQ0FBK0IsU0FBL0I7QUFDQSxpQkFBSSxDQUFDLFdBQUwsRUFBa0I7QUFDakIsZ0JBQUUsYUFBRixFQUFpQixHQUFqQixDQUFxQjtBQUNwQixxQkFBTSxTQUFTLGNBQVQsQ0FBd0IsYUFBYSxRQUFNLENBQW5CLENBQXhCLEVBQStDLHFCQUEvQyxHQUF1RSxJQUF2RSxHQUE4RSxFQUFFLGNBQWMsUUFBTSxDQUFwQixDQUFGLEVBQTBCLEtBQTFCLEtBQWtDO0FBRGxHLGVBQXJCO0FBR0E7QUFDRDs7QUFFRDtBQXpDSyxpQkEwQ0EsSUFBRyxTQUFTLENBQVQsSUFBYyxhQUFZLElBQTdCLEVBQWtDO0FBQ3RDLGdCQUFFLDBCQUFGLEVBQThCLEtBQTlCLENBQW9DLEdBQXBDLEVBQXlDLE9BQXpDLENBQWlEO0FBQ2hELDBCQUFXLEtBRHFDO0FBRWhELHdCQUFTO0FBRnVDLGVBQWpELEVBR0csRUFISDtBQUlBLGdCQUFFLDBCQUFGLEVBQThCLEtBQTlCLENBQW9DLEdBQXBDLEVBQXlDLE9BQXpDLENBQWlEO0FBQ2hELDJCQUFZLEtBRG9DO0FBRWhELHdCQUFTO0FBRnVDLGVBQWpELEVBR0csRUFISDtBQUlBLGdCQUFFLCtCQUFGLEVBQW1DLEtBQW5DLENBQXlDLElBQXpDLEVBQStDLE9BQS9DLENBQXVEO0FBQ3RELDJCQUFZLEtBRDBDO0FBRXRELHdCQUFTO0FBRjZDLGVBQXZELEVBR0csR0FISDtBQUlBLGdCQUFFLDBCQUFGLEVBQThCLEtBQTlCLENBQW9DLEdBQXBDLEVBQXlDLE9BQXpDLENBQWlEO0FBQ2hELDBCQUFXLEdBRHFDO0FBRWhELHdCQUFTO0FBRnVDLGVBQWpELEVBR0csR0FISDtBQUlBLGdCQUFFLDBCQUFGLEVBQThCLEtBQTlCLENBQW9DLEdBQXBDLEVBQXlDLE9BQXpDLENBQWlEO0FBQ2hELDJCQUFZLEdBRG9DO0FBRWhELHdCQUFTO0FBRnVDLGVBQWpELEVBR0csR0FISDtBQUlBLGdCQUFFLHFDQUFGLEVBQXlDLElBQXpDLENBQThDLFVBQVMsS0FBVCxFQUFnQixLQUFoQixFQUF1QjtBQUNwRSxtQkFBSSxhQUFhLEVBQUUsSUFBRixFQUFRLElBQVIsR0FBZSxNQUFmLEdBQXdCLElBQXhCLEdBQStCLEVBQUUseUJBQUYsRUFBNkIsS0FBN0IsR0FBcUMsTUFBckMsR0FBOEMsSUFBOUY7QUFDQyxpQkFBRSxJQUFGLEVBQVEsR0FBUixDQUFZLGFBQVosRUFBMkIsVUFBM0I7QUFDRCxlQUhEO0FBSUEseUJBQVcsWUFBVztBQUNyQixtQkFBSSxPQUFPLEVBQUUsSUFBRixFQUFRLEdBQVIsQ0FBWSwyQkFBWixDQUFYO0FBQ1ksMEJBQVcsWUFBVztBQUM3QixxQkFBSyxHQUFMLENBQVM7QUFDQSwwQkFBUyxHQURUO0FBRUEsNEJBQVc7QUFGWCxpQkFBVDtBQUlELGdCQUxRLEVBS04sUUFBTSxFQUxBO0FBTUgsZUFSVixFQVFZLEdBUlo7QUFTQSxnQkFBRSxrQkFBRixFQUFzQixXQUF0QixDQUFrQyxTQUFsQztBQUNBLGtCQUFJLENBQUMsV0FBTCxFQUFrQjtBQUNqQixpQkFBRSxhQUFGLEVBQWlCLEdBQWpCLENBQXFCO0FBQ3BCLHNCQUFNLFNBQVMsY0FBVCxDQUF3QixhQUFhLFFBQU0sQ0FBbkIsQ0FBeEIsRUFBK0MscUJBQS9DLEdBQXVFLElBQXZFLEdBQThFLEVBQUUsY0FBYyxRQUFNLENBQXBCLENBQUYsRUFBMEIsS0FBMUIsS0FBa0M7QUFEbEcsZ0JBQXJCO0FBR0E7QUFDRCxnQkFBRSxFQUFGLENBQUssUUFBTCxDQUFjLGlCQUFkLENBQWdDLElBQWhDLEVBQXNDLFVBQXRDO0FBQ0E7O0FBRUQ7QUEzQ0ssa0JBNENBLElBQUcsU0FBUyxDQUFULElBQWMsYUFBWSxNQUE3QixFQUFvQztBQUN4QyxpQkFBRSwwQkFBRixFQUE4QixLQUE5QixDQUFvQyxJQUFwQyxFQUEwQyxPQUExQyxDQUFrRDtBQUNqRCwyQkFBVyxLQURzQztBQUVqRCx5QkFBUztBQUZ3QyxnQkFBbEQsRUFHRyxFQUhIO0FBSUEsaUJBQUUsMEJBQUYsRUFBOEIsS0FBOUIsQ0FBb0MsSUFBcEMsRUFBMEMsT0FBMUMsQ0FBa0Q7QUFDakQsNEJBQVksS0FEcUM7QUFFakQseUJBQVM7QUFGd0MsZ0JBQWxELEVBR0csRUFISDtBQUlBLGlCQUFFLCtCQUFGLEVBQW1DLEtBQW5DLENBQXlDLElBQXpDLEVBQStDLE9BQS9DLENBQXVEO0FBQ3RELDRCQUFZLEtBRDBDO0FBRXRELHlCQUFTO0FBRjZDLGdCQUF2RCxFQUdHLEVBSEg7QUFJQSxpQkFBRSw0QkFBRixFQUFnQyxLQUFoQyxDQUFzQyxHQUF0QyxFQUEyQyxPQUEzQyxDQUFtRDtBQUNsRCw0QkFBWSxHQURzQztBQUVsRCx5QkFBUztBQUZ5QyxnQkFBbkQsRUFHRyxHQUhIO0FBSUEsaUJBQUUsNEJBQUYsRUFBZ0MsS0FBaEMsQ0FBc0MsR0FBdEMsRUFBMkMsT0FBM0MsQ0FBbUQ7QUFDbEQsNkJBQWEsR0FEcUM7QUFFbEQseUJBQVM7QUFGeUMsZ0JBQW5ELEVBR0csR0FISDtBQUlBLGlCQUFFLGtCQUFGLEVBQXNCLFFBQXRCLENBQStCLFNBQS9CO0FBQ0EsbUJBQUksQ0FBQyxXQUFMLEVBQWtCO0FBQ2pCLGtCQUFFLGFBQUYsRUFBaUIsR0FBakIsQ0FBcUI7QUFDcEIsdUJBQU0sU0FBUyxjQUFULENBQXdCLGFBQWEsUUFBTSxDQUFuQixDQUF4QixFQUErQyxxQkFBL0MsR0FBdUUsSUFBdkUsR0FBOEUsRUFBRSxjQUFjLFFBQU0sQ0FBcEIsQ0FBRixFQUEwQixLQUExQixLQUFrQztBQURsRyxpQkFBckI7QUFHQTs7QUFFRCxpQkFBRSxFQUFGLENBQUssUUFBTCxDQUFjLGlCQUFkLENBQWdDLEtBQWhDLEVBQXVDLE1BQXZDLEVBNUJ3QyxDQTRCUTtBQUNoRDs7QUFFRDtBQS9CSyxtQkFnQ0EsSUFBRyxTQUFTLENBQVQsSUFBYyxhQUFZLElBQTdCLEVBQWtDO0FBQ3RDLGtCQUFFLDRCQUFGLEVBQWdDLEtBQWhDLENBQXNDLEdBQXRDLEVBQTJDLE9BQTNDLENBQW1EO0FBQ2xELDZCQUFZLEtBRHNDO0FBRWxELDBCQUFTO0FBRnlDLGlCQUFuRCxFQUdHLEVBSEg7QUFJQSxrQkFBRSw0QkFBRixFQUFnQyxLQUFoQyxDQUFzQyxHQUF0QyxFQUEyQyxPQUEzQyxDQUFtRDtBQUNsRCw4QkFBYSxLQURxQztBQUVsRCwwQkFBUztBQUZ5QyxpQkFBbkQsRUFHRyxFQUhIO0FBSUEsa0JBQUUsMEJBQUYsRUFBOEIsS0FBOUIsQ0FBb0MsR0FBcEMsRUFBeUMsT0FBekMsQ0FBaUQ7QUFDaEQsNEJBQVcsR0FEcUM7QUFFaEQsMEJBQVM7QUFGdUMsaUJBQWpELEVBR0csR0FISDtBQUlBLGtCQUFFLDBCQUFGLEVBQThCLEtBQTlCLENBQW9DLEdBQXBDLEVBQXlDLE9BQXpDLENBQWlEO0FBQ2hELDZCQUFZLEdBRG9DO0FBRWhELDBCQUFTO0FBRnVDLGlCQUFqRCxFQUdHLEdBSEg7QUFJQSxrQkFBRSwrQkFBRixFQUFtQyxLQUFuQyxDQUF5QyxJQUF6QyxFQUErQyxPQUEvQyxDQUF1RDtBQUN0RCw2QkFBWSxHQUQwQztBQUV0RCwwQkFBUztBQUY2QyxpQkFBdkQsRUFHRyxHQUhIO0FBSUEsa0JBQUUsa0JBQUYsRUFBc0IsV0FBdEIsQ0FBa0MsU0FBbEM7QUFDQSxvQkFBSSxDQUFDLFdBQUwsRUFBa0I7QUFDakIsbUJBQUUsYUFBRixFQUFpQixHQUFqQixDQUFxQjtBQUNwQix3QkFBTSxTQUFTLGNBQVQsQ0FBd0IsYUFBYSxRQUFNLENBQW5CLENBQXhCLEVBQStDLHFCQUEvQyxHQUF1RSxJQUF2RSxHQUE4RSxFQUFFLGNBQWMsUUFBTSxDQUFwQixDQUFGLEVBQTBCLEtBQTFCLEtBQWtDO0FBRGxHLGtCQUFyQjtBQUdBO0FBQ0Qsa0JBQUUsRUFBRixDQUFLLFFBQUwsQ0FBYyxpQkFBZCxDQUFnQyxJQUFoQyxFQUFzQyxVQUF0QztBQUNBOztBQUVEO0FBOUJLLG9CQStCQSxJQUFHLFNBQVMsQ0FBVCxJQUFjLGFBQVksSUFBN0IsRUFBa0M7QUFDdEMsbUJBQUUsMEJBQUYsRUFBOEIsS0FBOUIsQ0FBb0MsSUFBcEMsRUFBMEMsT0FBMUMsQ0FBa0Q7QUFDakQsMkJBQVM7QUFEd0Msa0JBQWxELEVBRUcsRUFGSDtBQUdBLG1CQUFFLDZCQUFGLEVBQWlDLEtBQWpDLENBQXVDLElBQXZDLEVBQTZDLE9BQTdDLENBQXFEO0FBQ3BELDhCQUFZO0FBRHdDLGtCQUFyRCxFQUVHLEVBRkg7QUFHQSxtQkFBRSwwQkFBRixFQUE4QixLQUE5QixDQUFvQyxJQUFwQyxFQUEwQyxPQUExQyxDQUFrRDtBQUNqRCw4QkFBWSxLQURxQztBQUVqRCwyQkFBUztBQUZ3QyxrQkFBbEQsRUFHRyxFQUhIO0FBSUEsbUJBQUUsbUNBQUYsRUFBdUMsS0FBdkMsQ0FBNkMsSUFBN0MsRUFBbUQsT0FBbkQsQ0FBMkQ7QUFDMUQsOEJBQVksS0FEOEM7QUFFMUQsMkJBQVM7QUFGaUQsa0JBQTNELEVBR0csRUFISDtBQUlBLG1CQUFFLDJCQUFGLEVBQStCLEtBQS9CLENBQXFDLElBQXJDLEVBQTJDLE9BQTNDLENBQW1EO0FBQ2xELHVCQUFLLEtBRDZDO0FBRWxELDJCQUFTO0FBRnlDLGtCQUFuRCxFQUdHLEVBSEg7QUFJQSxtQkFBRSw0QkFBRixFQUFnQyxLQUFoQyxDQUFzQyxHQUF0QyxFQUEyQyxPQUEzQyxDQUFtRDtBQUNsRCw4QkFBWSxHQURzQztBQUVsRCwyQkFBUztBQUZ5QyxrQkFBbkQsRUFHRyxHQUhIO0FBSUEsbUJBQUUsNEJBQUYsRUFBZ0MsS0FBaEMsQ0FBc0MsR0FBdEMsRUFBMkMsT0FBM0MsQ0FBbUQ7QUFDbEQsK0JBQWEsR0FEcUM7QUFFbEQsMkJBQVM7QUFGeUMsa0JBQW5ELEVBR0csR0FISDtBQUlBLG1CQUFFLGtCQUFGLEVBQXNCLFFBQXRCLENBQStCLFNBQS9CO0FBQ0EscUJBQUksQ0FBQyxXQUFMLEVBQWtCO0FBQ2pCLG9CQUFFLGFBQUYsRUFBaUIsR0FBakIsQ0FBcUI7QUFDcEIseUJBQU0sU0FBUyxjQUFULENBQXdCLGFBQWEsUUFBTSxDQUFuQixDQUF4QixFQUErQyxxQkFBL0MsR0FBdUUsSUFBdkUsR0FBOEUsRUFBRSxjQUFjLFFBQU0sQ0FBcEIsQ0FBRixFQUEwQixLQUExQixLQUFrQztBQURsRyxtQkFBckI7QUFHQTtBQUNEOztBQUVELGtCQUFjLEtBQWQ7QUFDQTtBQWhwQnNCLEdBQXhCOztBQW1wQkEsV0FBUyxjQUFULENBQXdCLFVBQXhCLEVBQW9DLGdCQUFwQyxDQUFxRCxPQUFyRCxFQUE4RCxVQUFTLENBQVQsRUFBWTtBQUFFO0FBQ3JFLE9BQUksRUFBRSxNQUFGLEdBQVcsQ0FBZixFQUFrQjtBQUFFO0FBQ25CLGFBQVMsY0FBVCxDQUF3QixZQUF4QixFQUFzQyxnQkFBdEMsQ0FBdUQsT0FBdkQsRUFBZ0UsVUFBUyxDQUFULEVBQVk7QUFDakYsU0FBSSxFQUFFLE1BQUYsR0FBVyxDQUFmLEVBQWtCO0FBQ2pCLFFBQUUsRUFBRixDQUFLLFFBQUwsQ0FBYyxNQUFkLENBQXFCLFFBQXJCO0FBQ0E7QUFDQTtBQUNELEtBTEs7QUFNTixNQUFFLEVBQUYsQ0FBSyxRQUFMLENBQWMsYUFBZDtBQUNNO0FBQ0QsT0FBSSxFQUFFLE1BQUYsR0FBVyxDQUFmLEVBQWtCO0FBQUU7QUFDaEIsYUFBUyxjQUFULENBQXdCLFlBQXhCLEVBQXNDLGdCQUF0QyxDQUF1RCxPQUF2RCxFQUFnRSxVQUFTLENBQVQsRUFBWTtBQUNwRixTQUFJLEVBQUUsTUFBRixHQUFXLENBQWYsRUFBa0I7QUFDakIsUUFBRSxFQUFGLENBQUssUUFBTCxDQUFjLE1BQWQsQ0FBcUIsUUFBckI7QUFDQTtBQUNBO0FBQ0QsS0FMUTtBQU1ULE1BQUUsRUFBRixDQUFLLFFBQUwsQ0FBYyxjQUFkO0FBQ007QUFDUCxHQW5CRDs7QUFxQkEsSUFBRSxTQUFGLEVBQWEsS0FBYixDQUFtQixVQUFTLEtBQVQsRUFBZ0I7QUFDbEMsT0FBSSxLQUFLLEVBQUUsSUFBRixFQUFRLElBQVIsQ0FBYSxJQUFiLENBQVQ7QUFDQSxpQkFBYyxJQUFkO0FBQ0EsS0FBRSxhQUFGLEVBQWlCLEdBQWpCLENBQXFCO0FBQ3BCLFVBQU0sU0FBUyxjQUFULENBQXdCLEVBQXhCLEVBQTRCLHFCQUE1QixHQUFvRCxJQUFwRCxHQUEyRCxFQUFFLE1BQUksRUFBTixFQUFVLEtBQVYsS0FBa0I7QUFEL0QsSUFBckI7QUFHQSxHQU5EO0FBT0E7O0FBRUQ7QUFDQSxLQUFJLG9CQUFKLEVBQTBCO0FBQ3pCLElBQUUsZ0JBQUYsRUFBb0IsV0FBcEIsQ0FBZ0MsU0FBaEM7QUFDQSxJQUFFLGdDQUFGLEVBQW9DLFdBQXBDLENBQWdELFFBQWhEO0FBQ0EsSUFBRSxrQ0FBRixFQUFzQyxXQUF0QyxDQUFrRCxVQUFsRDtBQUNBLEVBSkQsTUFLSyxJQUFJLENBQUMsb0JBQUwsRUFBMEI7QUFDOUIsSUFBRSxTQUFGLEVBQWEsR0FBYixDQUFpQixTQUFqQixFQUE0QixNQUE1QjtBQUNBOztBQUVEO0FBQ0EsR0FBRSx5QkFBRixFQUE2QixLQUE3QixDQUFtQyxZQUFXO0FBQzdDLFFBQU0sY0FBTjtBQUNBLE1BQUksRUFBRSxJQUFGLEVBQVEsUUFBUixDQUFpQixVQUFqQixDQUFKLEVBQWtDO0FBQ2pDLEtBQUUsZ0NBQUYsRUFBb0MsUUFBcEMsQ0FBNkMsVUFBN0M7QUFDQSxLQUFFLGdDQUFGLEVBQW9DLFdBQXBDLENBQWdELFFBQWhEO0FBQ0EsS0FBRSxJQUFGLEVBQVEsUUFBUixDQUFpQixRQUFqQjtBQUNBLEtBQUUsSUFBRixFQUFRLFdBQVIsQ0FBb0IsVUFBcEI7O0FBR0EsS0FBRSxrQ0FBRixFQUNFLFFBREYsQ0FDVyxVQURYLEVBRUUsV0FGRixDQUVjLFFBRmQ7O0FBSUEsT0FBSSxFQUFFLElBQUYsRUFBUSxJQUFSLEdBQWUsUUFBZixDQUF3QixVQUF4QixDQUFKLEVBQXlDO0FBQ3hDLE1BQUUsSUFBRixFQUFRLElBQVIsR0FDRSxXQURGLENBQ2MsVUFEZCxFQUVFLFFBRkYsQ0FFVyxRQUZYO0FBR0E7QUFDRDtBQUNELEVBbkJEOztBQXFCQTtBQUNBLEdBQUUsZ0JBQUYsRUFBb0IsS0FBcEIsQ0FBMEIsWUFBVztBQUNwQyxRQUFNLGNBQU47QUFDQSxJQUFFLElBQUYsRUFBUSxXQUFSLENBQW9CLFFBQXBCO0FBQ0EsSUFBRSxJQUFGLEVBQVEsV0FBUixDQUFvQixPQUFwQjtBQUNBLElBQUUsU0FBRixFQUFhLFdBQWIsQ0FBeUIsTUFBekIsRUFBaUMsWUFBVyxDQUFFLENBQTlDO0FBQ0EsRUFMRDs7QUFPQSxHQUFFLGdCQUFGLEVBQW9CLEtBQXBCLENBQTBCLFlBQVc7QUFDcEMsUUFBTSxjQUFOO0FBQ0EsSUFBRSxnQkFBRixFQUFvQixXQUFwQixDQUFnQyxTQUFoQztBQUNBLElBQUUsZ0JBQUYsRUFBb0IsV0FBcEIsQ0FBZ0MsUUFBaEM7QUFDQSxJQUFFLGdCQUFGLEVBQW9CLFdBQXBCLENBQWdDLE9BQWhDO0FBQ0EsSUFBRSxJQUFGLEVBQVEsUUFBUixDQUFpQixTQUFqQjtBQUNBLElBQUUsU0FBRixFQUFhLFdBQWIsQ0FBeUIsTUFBekIsRUFBaUMsWUFBVyxDQUFFLENBQTlDO0FBQ0EsSUFBRSxZQUFGLEVBQWdCLE9BQWhCLENBQXdCO0FBQ2pCLGNBQVcsRUFBRSxFQUFFLElBQUYsRUFBUSxJQUFSLENBQWEsTUFBYixDQUFGLEVBQXdCLE1BQXhCLEdBQWlDLEdBQWpDLEdBQXVDLEVBQUUsUUFBRixFQUFZLE1BQVo7QUFEakMsR0FBeEIsRUFFTSxJQUZOO0FBR0EsRUFWRDtBQVdBLENBM3pCRCIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCIkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpe1xuXHR2YXIgcmVzcG9uc2l2ZU1vZGVUZWJsZXQgPSBmYWxzZTtcblx0dmFyIHJlc3BvbnNpdmVNb2RlRnVsbEhEID0gZmFsc2U7XG5cdHZhciBhbmNob3JDbGljayA9IGZhbHNlO1xuXHR2YXIgU0xJREVTX0NPVU5UID0gODtcblxuXHRNb2Rlcm5penIub24oJ3dlYnAnLCBmdW5jdGlvbihyZXN1bHQpIHtcblx0ICBpZiAocmVzdWx0KSB7XG5cdCAgICAkKCcuc2VjdGlvbicpLmFkZENsYXNzKCd3ZWJwJyk7XG5cdCAgfSBlbHNlIHtcblx0ICAgIGNvbnNvbGUubG9nKCdXZWJQIGlzIG5vdCBzdXBwb3J0ZWQhJyk7XG5cdCAgfVxuXHR9KTtcblxuXHRpZiAod2luZG93Lm1hdGNoTWVkaWEoJyhtYXgtd2lkdGg6IDc2OHB4KScpLm1hdGNoZXMpIHtcblx0XHRyZXNwb25zaXZlTW9kZVRlYmxldCA9IHRydWU7XG5cdH1cblxuXHRpZiAod2luZG93Lm1hdGNoTWVkaWEoJyhtaW4td2lkdGg6IDE5MjBweCknKS5tYXRjaGVzKSB7XG5cdFx0cmVzcG9uc2l2ZU1vZGVGdWxsSEQgPSB0cnVlO1xuXHR9XG5cblx0d2luZG93Lm9ucmVzaXplID0gZnVuY3Rpb24oKSB7XG5cdFx0aWYgKHdpbmRvdy5tYXRjaE1lZGlhKCcobWF4LXdpZHRoOiA3NjhweCknKS5tYXRjaGVzKSB7XG5cdFx0XHRyZXNwb25zaXZlTW9kZVRlYmxldCA9IHRydWU7XG5cdFx0fVxuXG5cdFx0ZWxzZSBpZiAod2luZG93Lm1hdGNoTWVkaWEoJyhtaW4td2lkdGg6IDc2OHB4KScpLm1hdGNoZXMpIHtcblx0XHRcdHJlc3BvbnNpdmVNb2RlVGVibGV0ID0gZmFsc2U7XG5cdFx0fVxuXG5cdFx0aWYgKHdpbmRvdy5tYXRjaE1lZGlhKCcobWluLXdpZHRoOiAxOTIwcHgpJykubWF0Y2hlcykge1xuXHRcdFx0cmVzcG9uc2l2ZU1vZGVGdWxsSEQgPSB0cnVlO1xuXHRcdH1cblxuXHRcdGVsc2UgaWYgKHdpbmRvdy5tYXRjaE1lZGlhKCcobWF4LXdpZHRoOiAxOTIwcHgpJykubWF0Y2hlcykge1xuXHRcdFx0cmVzcG9uc2l2ZU1vZGVGdWxsSEQgPSBmYWxzZTtcblx0XHR9XG5cdH1cblxuXHQkKCcuc3Vic2NyaWJlLWZvcm1fX2FncmVlJykuY2xpY2soZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpXG5cdFx0aWYgKCQoJy5zdWJzY3JpYmUtZm9ybV9fYWdyZWUtY2hlY2tib3gnKS5hdHRyKCdjaGVja2VkJykpIHtcblx0ICAgICAgICAkKCcuc3Vic2NyaWJlLWZvcm1fX2FncmVlLWNoZWNrYm94JykucmVtb3ZlQXR0cignY2hlY2tlZCcpXG5cdCAgICB9IGVsc2Uge1xuXHQgICAgICAgICQoJy5zdWJzY3JpYmUtZm9ybV9fYWdyZWUtY2hlY2tib3gnKS5hdHRyKCdjaGVja2VkJywgJ2NoZWNrZWQnKTtcblx0ICAgIH1cblx0fSk7XG5cblx0ZnVuY3Rpb24gc2V0QW5jaG9yQWN0aXZlKGluZGV4KSB7XG5cdFx0Zm9yIChpID0gU0xJREVTX0NPVU5UOyBpPj1pbmRleDsgaS0tKSB7XG5cdFx0XHQkKCdbaHJlZj1cIiNzbGlkZScgKyBpICsgJ1wiXScpLnJlbW92ZUNsYXNzKCd2aXNpdGVkJyk7XG5cdFx0fVxuXHRcdGZvciAoaSA9IDE7IGk8aW5kZXg7IGkrKykge1xuXHRcdFx0JCgnW2hyZWY9XCIjc2xpZGUnICsgaSArICdcIl0nKS5hZGRDbGFzcygndmlzaXRlZCcpO1xuXHRcdH1cblx0fVxuXG5cdGZ1bmN0aW9uIGFuaW1hdGVMaXN0RWxlbWVudHMoZWxlbWVudCwgZGVsYXksIHNwZWVkLCBtYXJnaW4sIGludGVydmFsLCBkaXJlY3Rpb24pIHtcblx0XHRlbGVtZW50LmNoaWxkcmVuKCkuZGVsYXkoZGVsYXkpLmVhY2goZnVuY3Rpb24oaW5kZXgsIHZhbHVlKXtcblx0XHRcdGlmIChkaXJlY3Rpb24gPT0gJ2xlZnQnKSB7XG5cdFx0XHRcdCQodGhpcykuZGVsYXkoaW50ZXJ2YWwgKiBpbmRleCkuYW5pbWF0ZSh7XG4gICAgICAgICAgICAgICAgXHRvcGFjaXR5OiAnMScsXG4gICAgICAgICAgICAgICAgXHRtYXJnaW5MZWZ0OiBtYXJnaW5cbiAgICAgICAgICAgIFx0fSwgc3BlZWQsICdzd2luZycpO1xuXHRcdFx0fVxuXHRcdFx0ZWxzZSBpZiAoZGlyZWN0aW9uID09ICdyaWdodCcpIHtcblx0XHRcdFx0JCh0aGlzKS5kZWxheShpbnRlcnZhbCAqIGluZGV4KS5hbmltYXRlKHtcbiAgICAgICAgICAgICAgICBcdG9wYWNpdHk6ICcwJyxcbiAgICAgICAgICAgICAgICBcdG1hcmdpbkxlZnQ6IG1hcmdpblxuICAgICAgICAgICAgXHR9LCBzcGVlZCwgJ3N3aW5nJyk7XG5cdFx0XHR9XG4gICAgICAgIH0pO1xuXHR9XG5cblx0ZnVuY3Rpb24gc2V0TmF2U2xpZGVyUG9zaXRpb24oKSB7XG5cdFx0JCgnLnNlY3Rpb24nKS5lYWNoKGZ1bmN0aW9uKGluZGV4LCB2YWx1ZSl7XG5cdFx0XHRpZiAoJCh0aGlzKS5oYXNDbGFzcygnYWN0aXZlJykpIHtcblx0XHRcdFx0dmFyIGkgPSAnYW5jaG9yLScrKGluZGV4KzEpO1xuXHRcdFx0XHQkKCcubmF2LWFjdGl2ZScpLmNzcyh7XG5cdFx0XHRcdFx0bGVmdDogZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoaSkuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkubGVmdCArICQoJyMnK2kpLndpZHRoKCkvMlxuXHRcdFx0XHR9KTtcblx0XHRcdH1cblx0XHR9KTtcblx0fVxuXG5cdGlmICghcmVzcG9uc2l2ZU1vZGVUZWJsZXQpIHtcblx0XHQkKCcjZnVsbHBhZ2UnKS5mdWxscGFnZSh7IC8v0LjQvdC40YbQuNCw0LvQuNC30LjRgNGD0LXQvCBmdWxscGFnZS5qc1xuXHRcdFx0c2Nyb2xsaW5nU3BlZWQ6IDEwMDAsXG5cdFx0XHRtZW51OiAnI21lbnUnLFxuXHRcdFx0YXV0b1Njcm9sbGluZzogdHJ1ZSxcblx0XHRcdGxhenlMb2FkaW5nOiBmYWxzZSxcblx0XHRcdGFuY2hvcnM6IFsnc2xpZGUwJywgJ3NsaWRlMScsICdzbGlkZTInLCAnc2xpZGUzJywgJ3NsaWRlNCcsICdzbGlkZTUnLCAnc2xpZGU2JywgJ3NsaWRlNyddLFxuXHRcdFx0ZWFzaW5nY3NzMzogJ2Vhc2UtaW4tb3V0JyxcblxuXHRcdFx0YWZ0ZXJMb2FkOiBmdW5jdGlvbihhbmNob3JMaW5rLCBpbmRleCl7XG5cdFx0XHRcdHZhciB0b3BNYXJnaW49JzEwMCc7XG5cdFx0XHRcdGlmIChyZXNwb25zaXZlTW9kZUZ1bGxIRCkge1xuXHRcdFx0XHRcdHRvcE1hcmdpbj0nMjUlJztcblx0XHRcdFx0fVxuXHRcdFx0XHRlbHNlIHtcblx0XHRcdFx0XHR0b3BNYXJnaW49JzEwMCc7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHQvL9C+0YLQutGA0YvRgtC40LUg0YHQu9Cw0LnQtNCwIDFcblx0XHRcdFx0aWYoaW5kZXggPT0gMSl7XG5cdFx0XHRcdFx0JCgnW2hyZWY9XCIjc2xpZGUwXCJdJykuYWRkQ2xhc3MoJ3Zpc2l0ZWQnKTtcblx0XHRcdFx0XHRmb3IgKGkgPSBTTElERVNfQ09VTlQ7IGk+PWluZGV4OyBpLS0pIHtcblx0XHRcdFx0XHRcdCQoJ1tocmVmPVwiI3NsaWRlJyArIGkgKyAnXCJdJykucmVtb3ZlQ2xhc3MoJ3Zpc2l0ZWQnKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb24wIC5zbGlkZV9faGVhZGVyJykuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRtYXJnaW5MZWZ0OiAnMCcsXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMSdcblx0XHRcdFx0XHR9LCA1MDApO1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uMCAuc3Vic2NyaWJlLWZvcm0nKS5kZWxheSgyMDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0dG9wOiB0b3BNYXJnaW4sXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMSdcblx0XHRcdFx0XHR9LCA1MDApO1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uMCAuYm9yZGVyLWJvdHRvbScpLmRlbGF5KDMwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRtYXJnaW5MZWZ0OiAnMCcsXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMSdcblx0XHRcdFx0XHR9LCA1MDApO1xuXHRcdFx0XHRcdHNldE5hdlNsaWRlclBvc2l0aW9uKClcblx0XHRcdFx0fVxuXG5cdFx0XHRcdGlmKGluZGV4ID09IDIpe1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uMSAuc2xpZGVfX2hlYWRlcicpLmRlbGF5KDUwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRtYXJnaW5Ub3A6ICcwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcxJ1xuXHRcdFx0XHRcdH0sIDUwMCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb24xIC5ib3JkZXItYm90dG9tJykuZGVsYXkoNzAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdG1hcmdpbkxlZnQ6ICcwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcxJ1xuXHRcdFx0XHRcdH0sIDUwMCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb24xIC52aWRlby13cmFwcGVyJykuZGVsYXkoNTAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdHRvcDogJzExJScsXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMSdcblx0XHRcdFx0XHR9LCA1MDApO1xuXHRcdFx0XHRcdCQoJ1tocmVmPVwiI3NsaWRlMVwiXScpLmFkZENsYXNzKCd2aXNpdGVkJyk7XG5cdFx0XHRcdFx0c2V0TmF2U2xpZGVyUG9zaXRpb24oKVxuXHRcdFx0XHRcdHNldEFuY2hvckFjdGl2ZShpbmRleCk7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRpZihpbmRleCA9PSAzKXtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjIgLnNsaWRlX19oZWFkZXInKS5kZWxheSg1MDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0bWFyZ2luVG9wOiAnMCcsXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMSdcblx0XHRcdFx0XHR9LCA1MDApO1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uMiAuYm9yZGVyLWJvdHRvbScpLmRlbGF5KDcwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRtYXJnaW5MZWZ0OiAnMCcsXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMSdcblx0XHRcdFx0XHR9LCA1MDApO1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uMiAuc2xpZGVfX2ltZy1yaWdodCcpLmRlbGF5KDcwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRtYXJnaW5Ub3A6ICcwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcxJ1xuXHRcdFx0XHRcdH0sIDUwMCk7XG5cdFx0XHRcdFx0YW5pbWF0ZUxpc3RFbGVtZW50cygkKCcjc2VjdGlvbjIgLnNsaWRlX19oZWFkZXJfX2xpc3QnKSwgODAwLCA1MDAsIDAsIDQwMCwgJ2xlZnQnKTtcblx0XHRcdFx0XHQkKCdbaHJlZj1cIiNzbGlkZTJcIl0nKS5hZGRDbGFzcygndmlzaXRlZCcpO1xuXHRcdFx0XHRcdHNldE5hdlNsaWRlclBvc2l0aW9uKClcblx0XHRcdFx0XHRzZXRBbmNob3JBY3RpdmUoaW5kZXgpO1xuXHRcdFx0XHR9XG5cblx0XHRcdFx0aWYoaW5kZXggPT0gNCl7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb24zIC5zbGlkZV9faGVhZGVyJykuZGVsYXkoNTAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdG1hcmdpblRvcDogJzAnLFxuXHRcdFx0XHRcdFx0b3BhY2l0eTogJzEnXG5cdFx0XHRcdFx0fSwgNTAwKTtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjMgLmJvcmRlci1ib3R0b20nKS5kZWxheSg3MDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0bWFyZ2luTGVmdDogJzAnLFxuXHRcdFx0XHRcdFx0b3BhY2l0eTogJzEnXG5cdFx0XHRcdFx0fSwgNTAwKTtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjMgLnNsaWRlX19pbWctcmlnaHQnKS5kZWxheSg3MDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0bWFyZ2luVG9wOiAnMCcsXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMSdcblx0XHRcdFx0XHR9LCA1MDApO1xuXHRcdFx0XHRcdGFuaW1hdGVMaXN0RWxlbWVudHMoJCgnI3NlY3Rpb24zIC5zbGlkZV9faGVhZGVyX19saXN0JyksIDgwMCwgNTAwLCAwLCA0MDAsICdsZWZ0Jyk7XG5cdFx0XHRcdFx0JCgnW2hyZWY9XCIjc2xpZGUzXCJdJykuYWRkQ2xhc3MoJ3Zpc2l0ZWQnKTtcblx0XHRcdFx0XHRzZXROYXZTbGlkZXJQb3NpdGlvbigpXG5cdFx0XHRcdFx0c2V0QW5jaG9yQWN0aXZlKGluZGV4KTtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdGlmKGluZGV4ID09IDUpe1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uNCAuc2xpZGVfX2hlYWRlcicpLmRlbGF5KDUwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRtYXJnaW5Ub3A6ICcwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcxJ1xuXHRcdFx0XHRcdH0sIDUwMCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb240IC5ib3JkZXItYm90dG9tJykuZGVsYXkoNzAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdG1hcmdpbkxlZnQ6ICcwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcxJ1xuXHRcdFx0XHRcdH0sIDUwMCk7XG5cdFx0XHRcdFx0JCgnLnNsaWRlX19pY29ucy13cmFwcGVyJykuY2hpbGRyZW4oKS5kZWxheSg3MDApLmVhY2goZnVuY3Rpb24oaW5kZXgsIHZhbHVlKXtcblx0XHRcdFx0XHRcdHZhciBzZWxmID0gJCh0aGlzKS5ub3QoJy5zbGlkZV9faWNvbnMtZGVzY3JpcHRpb24nKTtcblx0XHQgICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdFx0XHQgICAgXHRzZWxmLmNzcyh7XG5cdFx0XHQgICAgICAgICAgICAgICAgXHRvcGFjaXR5OiAnMScsXG5cdFx0XHQgICAgICAgICAgICAgICAgXHRtYXJnaW5Ub3A6ICcwJ1xuXHRcdCAgICAgICAgICAgICAgICBcdH0pO1xuXHRcdFx0XHRcdCAgICB9LCBpbmRleCo1MCk7XG5cdFx0ICAgICBcdFx0fSk7XG5cdFx0XHRcdFx0JCgnW2hyZWY9XCIjc2xpZGU0XCJdJykuYWRkQ2xhc3MoJ3Zpc2l0ZWQnKTtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjQgLnNsaWRlX19pY29ucy1kZXNjcmlwdGlvbicpLmVhY2goZnVuY3Rpb24oaW5kZXgsIHZhbHVlKSB7XG5cdFx0XHRcdFx0XHR2YXIgbWFyZ2luTGVmdCA9ICQodGhpcykucHJldigpLm9mZnNldCgpLmxlZnQgLSAkKCcuZnJhbmNoaXNlLWljb25fX2NpcmNsZScpLmZpcnN0KCkub2Zmc2V0KCkubGVmdDtcblx0XHRcdFx0XHRcdCAkKHRoaXMpLmNzcygnbWFyZ2luLWxlZnQnLCBtYXJnaW5MZWZ0KTtcblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRzZXROYXZTbGlkZXJQb3NpdGlvbigpXG5cdFx0XHRcdFx0c2V0QW5jaG9yQWN0aXZlKGluZGV4KTtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdGlmKGluZGV4ID09IDYpe1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uNSAuc2xpZGVfX2hlYWRlcicpLmRlbGF5KDUwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRtYXJnaW5Ub3A6ICcwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcxJ1xuXHRcdFx0XHRcdH0sIDUwMCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb241IC5ib3JkZXItYm90dG9tJykuZGVsYXkoNzAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdG1hcmdpbkxlZnQ6ICcwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcxJ1xuXHRcdFx0XHRcdH0sIDUwMCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb241IC5zbGlkZV9fZGVzY3JpcHRpb24nKS5kZWxheSgxMDAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdG1hcmdpbkxlZnQ6ICcwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcxJ1xuXHRcdFx0XHRcdH0sIDUwMCk7XG5cdFx0XHRcdFx0JCgnW2hyZWY9XCIjc2xpZGU1XCJdJykuYWRkQ2xhc3MoJ3Zpc2l0ZWQnKTtcblx0XHRcdFx0XHRzZXROYXZTbGlkZXJQb3NpdGlvbigpXG5cdFx0XHRcdFx0c2V0QW5jaG9yQWN0aXZlKGluZGV4KTtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdGlmKGluZGV4ID09IDcpe1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uNl8xIC5zbGlkZV9faGVhZGVyJykuZGVsYXkoNTAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdHBhZGRpbmdUb3A6ICcwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcxJ1xuXHRcdFx0XHRcdH0sIDUwMCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb242XzEgLmJvcmRlci1ib3R0b20nKS5kZWxheSg3MDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0cGFkZGluZ0xlZnQ6ICcwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcxJ1xuXHRcdFx0XHRcdH0sIDUwMCk7XG5cdFx0XHRcdFx0JCgnW2hyZWY9XCIjc2xpZGU2XCJdJykuYWRkQ2xhc3MoJ3Zpc2l0ZWQnKTtcblx0XHRcdFx0XHRzZXROYXZTbGlkZXJQb3NpdGlvbigpXG5cdFx0XHRcdFx0c2V0QW5jaG9yQWN0aXZlKGluZGV4KTtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdGlmKGluZGV4ID09IDgpe1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uNyAuc2xpZGVfX2hlYWRlciBoMScpLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0bWFyZ2luTGVmdDogJzAnXG5cdFx0XHRcdFx0fSwgNTAwKTtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjcgLnNsaWRlX19oZWFkZXInKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcxJ1xuXHRcdFx0XHRcdH0sIDUwMCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb243IC5zdWJzY3JpYmUtZm9ybScpLmRlbGF5KDIwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHR0b3A6IHRvcE1hcmdpbixcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcxJ1xuXHRcdFx0XHRcdH0sIDUwMCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb243IC5ib3JkZXItYm90dG9tJykuZGVsYXkoMzAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdG1hcmdpbkxlZnQ6ICcwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcxJ1xuXHRcdFx0XHRcdH0sIDUwMCk7XG5cdFx0XHRcdFx0YW5pbWF0ZUxpc3RFbGVtZW50cygkKCcjc2VjdGlvbjcgLnNsaWRlX19oZWFkZXJfX2xpc3QnKSwgODAwLCA1MDAsIDAsIDQwMCwgJ2xlZnQnKTtcblx0XHRcdFx0XHQkKCdbaHJlZj1cIiNzbGlkZTdcIl0nKS5hZGRDbGFzcygndmlzaXRlZCcpO1xuXHRcdFx0XHRcdHNldE5hdlNsaWRlclBvc2l0aW9uKClcblx0XHRcdFx0XHRzZXRBbmNob3JBY3RpdmUoaW5kZXgpO1xuXHRcdFx0XHR9XG5cblx0XHRcdFx0YW5jaG9yQ2xpY2sgPSBmYWxzZTtcblx0XHRcdH0sXG5cblx0XHRcdG9uTGVhdmU6IGZ1bmN0aW9uKGluZGV4LCBuZXh0SW5kZXgsIGRpcmVjdGlvbil7XG5cdFx0XHRcdC8v0L/QtdGA0LXRhdC+0LQg0YEgMSDQvdCwIDIg0YHQu9Cw0LnQtNC10YBcblx0XHRcdFx0aWYoaW5kZXggPT0gMSAmJiBkaXJlY3Rpb24gPT0nZG93bicpe1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uMCAuc2xpZGVfX2hlYWRlcicpLmRlbGF5KDEwMDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0bWFyZ2luTGVmdDogJzQwMCcsXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMCdcblx0XHRcdFx0XHR9LCA1MCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb24wIC5zdWJzY3JpYmUtZm9ybScpLmRlbGF5KDEyMDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0b3BhY2l0eTogJzAnfSkuZGVsYXkoMTAwKS5hbmltYXRlKHt0b3A6ICc0MDBweCd9KTtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjAgLmJvcmRlci1ib3R0b20nKS5kZWxheSgxMzAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdG1hcmdpbkxlZnQ6ICcxMDAnLFxuXHRcdFx0XHRcdFx0b3BhY2l0eTogJzAnXG5cdFx0XHRcdFx0fSwgNTApO1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uMSAuc2xpZGVfX2hlYWRlcicpLmRlbGF5KDUwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRtYXJnaW5Ub3A6ICcwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcxJ1xuXHRcdFx0XHRcdH0sIDUwMCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb24xIC5ib3JkZXItYm90dG9tJykuZGVsYXkoNzAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdG1hcmdpbkxlZnQ6ICcwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcxJ1xuXHRcdFx0XHRcdH0sIDUwMCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb24xIC52aWRlby13cmFwcGVyJykuc2hvdygnZmFzdCcsIGZ1bmN0aW9uKCl7fSkuZGVsYXkoNTAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdHRvcDogJzExJScsXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMSdcblx0XHRcdFx0XHR9LCAzMDApO1xuXHRcdFx0XHRcdCQoJ1tocmVmPVwiI3NsaWRlMVwiXScpLmFkZENsYXNzKCd2aXNpdGVkJyk7XG5cdFx0XHRcdFx0aWYgKCFhbmNob3JDbGljaykge1xuXHRcdFx0XHRcdFx0JCgnLm5hdi1hY3RpdmUnKS5jc3Moe1xuXHRcdFx0XHRcdFx0XHRsZWZ0OiBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnYW5jaG9yLScgKyAoaW5kZXgrMSkpLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLmxlZnQgKyAkKCcjYW5jaG9yLScgKyAoaW5kZXgrMSkpLndpZHRoKCkvMlxuXHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0fTtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdC8v0L/QtdGA0LXRhdC+0LQg0YHQviAyINC90LAgMSDRgdC70LDQudC00LXRgFxuXHRcdFx0XHRlbHNlIGlmKGluZGV4ID09IDIgJiYgZGlyZWN0aW9uID09J3VwJyl7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb24xIC5zbGlkZV9faGVhZGVyJykuZGVsYXkoMTAwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRtYXJnaW5Ub3A6ICc0MDAnLFxuXHRcdFx0XHRcdFx0b3BhY2l0eTogJzAnXG5cdFx0XHRcdFx0fSwgNTApO1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uMSAuYm9yZGVyLWJvdHRvbScpLmRlbGF5KDEwMDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0bWFyZ2luTGVmdDogJzEwMCcsXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMCdcblx0XHRcdFx0XHR9LCA1MCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb24xIC52aWRlby13cmFwcGVyJykuZGVsYXkoMTAwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHR0b3A6ICc5MCUnLFxuXHRcdFx0XHRcdFx0b3BhY2l0eTogJzAnXG5cdFx0XHRcdFx0fSwgNTApO1xuXHRcdFx0XHRcdCQoJ1tocmVmPVwiI3NsaWRlMVwiXScpLnJlbW92ZUNsYXNzKCd2aXNpdGVkJylcblx0XHRcdFx0XHRpZiAoIWFuY2hvckNsaWNrKSB7XG5cdFx0XHRcdFx0XHQkKCcubmF2LWFjdGl2ZScpLmNzcyh7XG5cdFx0XHRcdFx0XHRcdGxlZnQ6IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdhbmNob3ItJyArIChpbmRleC0xKSkuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkubGVmdCArICQoJyNhbmNob3ItJyArIChpbmRleC0xKSkud2lkdGgoKS8yXG5cdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHR9O1xuXHRcdFx0XHR9XG5cblx0XHRcdFx0Ly/Qv9C10YDQtdGF0L7QtCDRgdC+IDIg0L3QsCAzINGB0LvQsNC50LTQtdGAXG5cdFx0XHRcdGVsc2UgaWYoaW5kZXggPT0gMiAmJiBkaXJlY3Rpb24gPT0nZG93bicpe1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uMSAuc2xpZGVfX2hlYWRlcicpLmRlbGF5KDEwMDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0bWFyZ2luVG9wOiAnNDAwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcwJ1xuXHRcdFx0XHRcdH0sIDUwKTtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjEgLmJvcmRlci1ib3R0b20nKS5kZWxheSgxMDAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdG1hcmdpbkxlZnQ6ICcxMDAnLFxuXHRcdFx0XHRcdFx0b3BhY2l0eTogJzAnXG5cdFx0XHRcdFx0fSwgNTApO1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uMSAudmlkZW8td3JhcHBlcicpLmRlbGF5KDEwMDApLmhpZGUoJ2Zhc3QnLCBmdW5jdGlvbigpIHt9KS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdHRvcDogJzkwJScsXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMCdcblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjIgLnNsaWRlX19oZWFkZXInKS5kZWxheSg1MDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0bWFyZ2luVG9wOiAnMCcsXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMSdcblx0XHRcdFx0XHR9LCA1MCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb24yIC5ib3JkZXItYm90dG9tJykuZGVsYXkoNzAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdG1hcmdpbkxlZnQ6ICcwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcxJ1xuXHRcdFx0XHRcdH0sIDUwMCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb24yIC5zbGlkZV9faW1nLXJpZ2h0JykuZGVsYXkoNzAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdG1hcmdpblRvcDogJzAnLFxuXHRcdFx0XHRcdFx0b3BhY2l0eTogJzEnXG5cdFx0XHRcdFx0fSwgNTAwKTtcblx0XHRcdFx0XHRhbmltYXRlTGlzdEVsZW1lbnRzKCQoJyNzZWN0aW9uMiAuc2xpZGVfX2hlYWRlcl9fbGlzdCcpLCA4MDAsIDUwMCwgMCwgNDAwLCAnbGVmdCcpO1xuXHRcdFx0XHRcdCQoJ1tocmVmPVwiI3NsaWRlMlwiXScpLmFkZENsYXNzKCd2aXNpdGVkJyk7XG5cdFx0XHRcdFx0aWYgKCFhbmNob3JDbGljaykge1xuXHRcdFx0XHRcdFx0JCgnLm5hdi1hY3RpdmUnKS5jc3Moe1xuXHRcdFx0XHRcdFx0XHRsZWZ0OiBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnYW5jaG9yLScgKyAoaW5kZXgrMSkpLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLmxlZnQgKyAkKCcjYW5jaG9yLScgKyAoaW5kZXgrMSkpLndpZHRoKCkvMlxuXHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0fTtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdC8v0L/QtdGA0LXRhdC+0LQg0YEgMyDQvdCwIDIg0YHQu9Cw0LnQtNC10YBcblx0XHRcdFx0ZWxzZSBpZihpbmRleCA9PSAzICYmIGRpcmVjdGlvbiA9PSd1cCcpe1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uMSAuc2xpZGVfX2hlYWRlcicpLmRlbGF5KDUwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRtYXJnaW5Ub3A6ICcwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcxJ1xuXHRcdFx0XHRcdH0sIDUwMCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb24xIC5ib3JkZXItYm90dG9tJykuZGVsYXkoNzAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdG1hcmdpbkxlZnQ6ICcwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcxJ1xuXHRcdFx0XHRcdH0sIDUwMCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb24xIC52aWRlby13cmFwcGVyJykuZGVsYXkoNTAwKS5zaG93KCdmYXN0JywgZnVuY3Rpb24oKXt9KS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdHRvcDogJzExJScsXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMSdcblx0XHRcdFx0XHR9LCAzMDApO1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uMiAuc2xpZGVfX2hlYWRlcicpLmRlbGF5KDUwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRtYXJnaW5Ub3A6ICc0MDAnLFxuXHRcdFx0XHRcdFx0b3BhY2l0eTogJzAnXG5cdFx0XHRcdFx0fSwgNTApO1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uMiAuYm9yZGVyLWJvdHRvbScpLmRlbGF5KDcwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRtYXJnaW5MZWZ0OiAnMTAwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcwJ1xuXHRcdFx0XHRcdH0sIDUwKTtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjIgLnNsaWRlX19pbWctcmlnaHQnKS5kZWxheSg3MDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0bWFyZ2luVG9wOiAnNDAwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcwJ1xuXHRcdFx0XHRcdH0sIDUwKTtcblx0XHRcdFx0XHRhbmltYXRlTGlzdEVsZW1lbnRzKCQoJyNzZWN0aW9uMiAuc2xpZGVfX2hlYWRlcl9fbGlzdCcpLCA5NTAsIDEwLCAzMDAsIDAsICdyaWdodCcpO1xuXHRcdFx0XHRcdCQoJ1tocmVmPVwiI3NsaWRlMlwiXScpLnJlbW92ZUNsYXNzKCd2aXNpdGVkJylcblx0XHRcdFx0XHRpZiAoIWFuY2hvckNsaWNrKSB7XG5cdFx0XHRcdFx0XHQkKCcubmF2LWFjdGl2ZScpLmNzcyh7XG5cdFx0XHRcdFx0XHRcdGxlZnQ6IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdhbmNob3ItJyArIChpbmRleC0xKSkuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkubGVmdCArICQoJyNhbmNob3ItJyArIChpbmRleC0xKSkud2lkdGgoKS8yXG5cdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHR9O1xuXHRcdFx0XHR9XG5cblx0XHRcdFx0Ly/Qv9C10YDQtdGF0L7QtCDRgSAzINC90LAgNCDRgdC70LDQudC00LXRgFxuXHRcdFx0XHRlbHNlIGlmKGluZGV4ID09IDMgJiYgZGlyZWN0aW9uID09J2Rvd24nKXtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjIgLnNsaWRlX19oZWFkZXInKS5kZWxheSg1MDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0bWFyZ2luVG9wOiAnNDAwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcwJ1xuXHRcdFx0XHRcdH0sIDUwKTtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjIgLmJvcmRlci1ib3R0b20nKS5kZWxheSg3MDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0bWFyZ2luTGVmdDogJzEwMCcsXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMCdcblx0XHRcdFx0XHR9LCA1MCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb24yIC5zbGlkZV9faW1nLXJpZ2h0JykuZGVsYXkoNzAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdG1hcmdpblRvcDogJzQwMHB4Jyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcwJ1xuXHRcdFx0XHRcdH0sIDUwKTtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjIgLnNsaWRlX19oZWFkZXJfX2xpc3QgbGknKS5kZWxheSgxMDAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdG1hcmdpbkxlZnQ6ICczMDAnLFxuXHRcdFx0XHRcdFx0b3BhY2l0eTogJzAnXG5cdFx0XHRcdFx0fSwgMTApO1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uMyAuc2xpZGVfX2hlYWRlcicpLmRlbGF5KDUwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRtYXJnaW5Ub3A6ICcwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcxJ1xuXHRcdFx0XHRcdH0sIDUwMCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb24zIC5ib3JkZXItYm90dG9tJykuZGVsYXkoNzAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdG1hcmdpbkxlZnQ6ICcwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcxJ1xuXHRcdFx0XHRcdH0sIDUwMCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb24zIC5zbGlkZV9faW1nLXJpZ2h0JykuZGVsYXkoNzAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdG1hcmdpblRvcDogJzAnLFxuXHRcdFx0XHRcdFx0b3BhY2l0eTogJzEnXG5cdFx0XHRcdFx0fSwgNTAwKTtcblx0XHRcdFx0XHRhbmltYXRlTGlzdEVsZW1lbnRzKCQoJyNzZWN0aW9uMyAuc2xpZGVfX2hlYWRlcl9fbGlzdCcpLCA4MDAsIDUwMCwgMCwgNDAwLCAnbGVmdCcpO1xuXHRcdFx0XHRcdCQoJ1tocmVmPVwiI3NsaWRlM1wiXScpLmFkZENsYXNzKCd2aXNpdGVkJyk7XG5cdFx0XHRcdFx0aWYgKCFhbmNob3JDbGljaykge1xuXHRcdFx0XHRcdFx0JCgnLm5hdi1hY3RpdmUnKS5jc3Moe1xuXHRcdFx0XHRcdFx0XHRsZWZ0OiBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnYW5jaG9yLScgKyAoaW5kZXgrMSkpLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLmxlZnQgKyAkKCcjYW5jaG9yLScgKyAoaW5kZXgrMSkpLndpZHRoKCkvMlxuXHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0fTtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdC8v0L/QtdGA0LXRhdC+0LQg0YEgNCDQvdCwIDMg0YHQu9Cw0LnQtNC10YBcblx0XHRcdFx0ZWxzZSBpZihpbmRleCA9PSA0ICYmIGRpcmVjdGlvbiA9PSd1cCcpe1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uMyAuc2xpZGVfX2hlYWRlcicpLmRlbGF5KDUwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRtYXJnaW5Ub3A6ICc0MDAnLFxuXHRcdFx0XHRcdFx0b3BhY2l0eTogJzAnXG5cdFx0XHRcdFx0fSwgNTApO1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uMyAuYm9yZGVyLWJvdHRvbScpLmRlbGF5KDcwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRtYXJnaW5MZWZ0OiAnMTAwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcwJ1xuXHRcdFx0XHRcdH0sIDUwKTtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjMgLnNsaWRlX19pbWctcmlnaHQnKS5kZWxheSg3MDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0bWFyZ2luVG9wOiAnNDAwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcwJ1xuXHRcdFx0XHRcdH0sIDUwKTtcblx0XHRcdFx0XHRhbmltYXRlTGlzdEVsZW1lbnRzKCQoJyNzZWN0aW9uMyAuc2xpZGVfX2hlYWRlcl9fbGlzdCcpLCA5NTAsIDEwLCAzMDAsIDAsICdyaWdodCcpO1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uMiAuc2xpZGVfX2hlYWRlcicpLmRlbGF5KDUwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRtYXJnaW5Ub3A6ICcwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcxJ1xuXHRcdFx0XHRcdH0sIDUwMCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb24yIC5ib3JkZXItYm90dG9tJykuZGVsYXkoNzAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdG1hcmdpbkxlZnQ6ICcwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcxJ1xuXHRcdFx0XHRcdH0sIDUwMCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb24yIC5zbGlkZV9faW1nLXJpZ2h0JykuZGVsYXkoNzAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdG1hcmdpblRvcDogJzAnLFxuXHRcdFx0XHRcdFx0b3BhY2l0eTogJzEnXG5cdFx0XHRcdFx0fSwgNTAwKTtcblx0XHRcdFx0XHRhbmltYXRlTGlzdEVsZW1lbnRzKCQoJyNzZWN0aW9uMiAuc2xpZGVfX2hlYWRlcl9fbGlzdCcpLCA4MDAsIDUwMCwgMCwgNDAwLCAnbGVmdCcpO1xuXHRcdFx0XHRcdCQoJ1tocmVmPVwiI3NsaWRlM1wiXScpLnJlbW92ZUNsYXNzKCd2aXNpdGVkJylcblx0XHRcdFx0XHRpZiAoIWFuY2hvckNsaWNrKSB7XG5cdFx0XHRcdFx0XHQkKCcubmF2LWFjdGl2ZScpLmNzcyh7XG5cdFx0XHRcdFx0XHRcdGxlZnQ6IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdhbmNob3ItJyArIChpbmRleC0xKSkuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkubGVmdCArICQoJyNhbmNob3ItJyArIChpbmRleC0xKSkud2lkdGgoKS8yXG5cdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHR9O1xuXHRcdFx0XHR9XG5cblx0XHRcdFx0Ly/Qv9C10YDQtdGF0L7QtCDRgSA0INC90LAgNSDRgdC70LDQudC00LXRgFxuXHRcdFx0XHRlbHNlIGlmKGluZGV4ID09IDQgJiYgZGlyZWN0aW9uID09J2Rvd24nKXtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjMgLnNsaWRlX19oZWFkZXInKS5kZWxheSg1MDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0bWFyZ2luVG9wOiAnNDAwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcwJ1xuXHRcdFx0XHRcdH0sIDUwKTtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjMgLmJvcmRlci1ib3R0b20nKS5kZWxheSg3MDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0bWFyZ2luTGVmdDogJzEwMCcsXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMCdcblx0XHRcdFx0XHR9LCA1MCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb24zIC5zbGlkZV9faW1nLXJpZ2h0JykuZGVsYXkoNzAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdG1hcmdpblRvcDogJzEyMCcsXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMCdcblx0XHRcdFx0XHR9LCA1MCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb24zIC5zbGlkZV9faGVhZGVyX19saXN0IGxpJykuZGVsYXkoNTAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdG1hcmdpbkxlZnQ6ICczMjAnLFxuXHRcdFx0XHRcdFx0b3BhY2l0eTogJzAnXG5cdFx0XHRcdFx0fSwgNTAwKTtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjQgLnNsaWRlX19oZWFkZXInKS5kZWxheSg1MDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0bWFyZ2luVG9wOiAnMCcsXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMSdcblx0XHRcdFx0XHR9LCA1MDApO1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uNCAuYm9yZGVyLWJvdHRvbScpLmRlbGF5KDcwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRtYXJnaW5MZWZ0OiAnMCcsXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMSdcblx0XHRcdFx0XHR9LCA1MDApO1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uNCAuc2xpZGVfX2ljb25zLWRlc2NyaXB0aW9uJykuZWFjaChmdW5jdGlvbihpbmRleCwgdmFsdWUpIHtcblx0XHRcdFx0XHRcdHZhciBtYXJnaW5MZWZ0ID0gJCh0aGlzKS5wcmV2KCkub2Zmc2V0KCkubGVmdCAtICQoJy5mcmFuY2hpc2UtaWNvbl9fY2lyY2xlJykuZmlyc3QoKS5vZmZzZXQoKS5sZWZ0O1xuXHRcdFx0XHRcdFx0ICQodGhpcykuY3NzKCdtYXJnaW4tbGVmdCcsIG1hcmdpbkxlZnQpO1xuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdCQoJy5zbGlkZV9faWNvbnMtd3JhcHBlcicpLmNoaWxkcmVuKCkuZWFjaChmdW5jdGlvbihpbmRleCwgdmFsdWUpe1xuXHRcdFx0XHRcdHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHR2YXIgc2VsZiA9ICQodGhpcykubm90KCcuc2xpZGVfX2ljb25zLWRlc2NyaXB0aW9uJyk7XG5cdFx0ICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHQgICAgXHRzZWxmLmNzcyh7XG5cdFx0XHQgICAgICAgICAgICAgICAgXHRvcGFjaXR5OiAnMScsXG5cdFx0XHQgICAgICAgICAgICAgICAgXHRtYXJnaW5Ub3A6ICcwJ1xuXHRcdCAgICAgICAgICAgICAgICBcdH0pO1xuXHRcdFx0XHRcdCAgICB9LCBpbmRleCo1MCk7XG5cdFx0ICAgICAgICAgICAgfSwgNjAwKTtcblx0XHQgICAgICB9KTtcblx0XHRcdFx0XHQkKCdbaHJlZj1cIiNzbGlkZTRcIl0nKS5hZGRDbGFzcygndmlzaXRlZCcpO1xuXHRcdFx0XHRcdGlmICghYW5jaG9yQ2xpY2spIHtcblx0XHRcdFx0XHRcdCQoJy5uYXYtYWN0aXZlJykuY3NzKHtcblx0XHRcdFx0XHRcdFx0bGVmdDogZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2FuY2hvci0nICsgKGluZGV4KzEpKS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS5sZWZ0ICsgJCgnI2FuY2hvci0nICsgKGluZGV4KzEpKS53aWR0aCgpLzJcblx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdH07XG5cdFx0XHRcdH1cblxuXHRcdFx0XHQvL9C/0LXRgNC10YXQvtC0INGBIDUg0L3QsCA0INGB0LvQsNC50LTQtdGAXG5cdFx0XHRcdGVsc2UgaWYoaW5kZXggPT0gNSAmJiBkaXJlY3Rpb24gPT0ndXAnKXtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjQgLnNsaWRlX19oZWFkZXInKS5kZWxheSg1MDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0bWFyZ2luVG9wOiAnNDAwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcwJ1xuXHRcdFx0XHRcdH0sIDUwKTtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjQgLmJvcmRlci1ib3R0b20nKS5kZWxheSg3MDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0bWFyZ2luTGVmdDogJzEwMCcsXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMCdcblx0XHRcdFx0XHR9LCA1MCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb240IC5mcmFuY2hpc2UtaWNvbl9fY2lyY2xlJykuZGVsYXkoMTAwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRtYXJnaW5Ub3A6ICczMDAnLFxuXHRcdFx0XHRcdFx0b3BhY2l0eTogJzAnXG5cdFx0XHRcdFx0fSwgNTApO1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uNCAuZnJhbmNoaXNlLWljb24tZG90JykuZGVsYXkoMTAwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRtYXJnaW5Ub3A6ICczMDAnLFxuXHRcdFx0XHRcdFx0b3BhY2l0eTogJzAnXG5cdFx0XHRcdFx0fSwgNTApO1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uNCAuc2xpZGVfX2ljb25zLWRlc2NyaXB0aW9uJykuZGVsYXkoNzAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdG1hcmdpbkxlZnQ6ICc0MDAnLFxuXHRcdFx0XHRcdFx0b3BhY2l0eTogJzAnXG5cdFx0XHRcdFx0fSwgNTApO1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uMyAuc2xpZGVfX2hlYWRlcicpLmRlbGF5KDUwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRtYXJnaW5Ub3A6ICcwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcxJ1xuXHRcdFx0XHRcdH0sIDUwMCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb24zIC5ib3JkZXItYm90dG9tJykuZGVsYXkoNzAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdG1hcmdpbkxlZnQ6ICcwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcxJ1xuXHRcdFx0XHRcdH0sIDUwMCk7XG5cdFx0XHRcdFx0YW5pbWF0ZUxpc3RFbGVtZW50cygkKCcjc2VjdGlvbjMgLnNsaWRlX19oZWFkZXJfX2xpc3QnKSwgODAwLCA1MDAsIDAsIDQwMCwgJ2xlZnQnKTtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjMgLnNsaWRlX19pbWctcmlnaHQnKS5kZWxheSg3MDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0bWFyZ2luVG9wOiAnMCcsXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMSdcblx0XHRcdFx0XHR9LCA1MDApO1xuXG5cdFx0XHRcdFx0JCgnW2hyZWY9XCIjc2xpZGU0XCJdJykucmVtb3ZlQ2xhc3MoJ3Zpc2l0ZWQnKVxuXHRcdFx0XHRcdGlmICghYW5jaG9yQ2xpY2spIHtcblx0XHRcdFx0XHRcdCQoJy5uYXYtYWN0aXZlJykuY3NzKHtcblx0XHRcdFx0XHRcdFx0bGVmdDogZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2FuY2hvci0nICsgKGluZGV4LTEpKS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS5sZWZ0ICsgJCgnI2FuY2hvci0nICsgKGluZGV4LTEpKS53aWR0aCgpLzJcblx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdH07XG5cdFx0XHRcdH1cblxuXHRcdFx0XHQvL9C/0LXRgNC10YXQvtC0INGBIDUg0L3QsCA2INGB0LvQsNC50LTQtdGAXG5cdFx0XHRcdGVsc2UgaWYoaW5kZXggPT0gNSAmJiBkaXJlY3Rpb24gPT0nZG93bicpe1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uNCAuc2xpZGVfX2hlYWRlcicpLmRlbGF5KDEwMDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0bWFyZ2luVG9wOiAnNDAwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcwJ1xuXHRcdFx0XHRcdH0sIDUwKTtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjQgLmJvcmRlci1ib3R0b20nKS5kZWxheSgxMDAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdG1hcmdpbkxlZnQ6ICcxMDAnLFxuXHRcdFx0XHRcdFx0b3BhY2l0eTogJzAnXG5cdFx0XHRcdFx0fSwgNTApO1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uNCAuZnJhbmNoaXNlLWljb25fX2NpcmNsZScpLmRlbGF5KDEwMDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0bWFyZ2luVG9wOiAnMzAwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcwJ1xuXHRcdFx0XHRcdH0sIDUwKTtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjQgLmZyYW5jaGlzZS1pY29uLWRvdCcpLmRlbGF5KDEwMDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0bWFyZ2luVG9wOiAnMzAwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcwJ1xuXHRcdFx0XHRcdH0sIDUwKTtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjQgLnNsaWRlX19pY29ucy1kZXNjcmlwdGlvbicpLmRlbGF5KDcwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRtYXJnaW5MZWZ0OiAnNDAwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcwJ1xuXHRcdFx0XHRcdH0sIDUwKTtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjUgLnNsaWRlX19oZWFkZXInKS5kZWxheSg1MDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0bWFyZ2luVG9wOiAnMCcsXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMSdcblx0XHRcdFx0XHR9LCA1MDApO1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uNSAuYm9yZGVyLWJvdHRvbScpLmRlbGF5KDcwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRtYXJnaW5MZWZ0OiAnMCcsXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMSdcblx0XHRcdFx0XHR9LCA1MDApO1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uNSAuc2xpZGVfX2Rlc2NyaXB0aW9uJykuZGVsYXkoMTAwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRtYXJnaW5MZWZ0OiAnMCcsXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMSdcblx0XHRcdFx0XHR9LCA1MDApO1xuXHRcdFx0XHRcdCQoJ1tocmVmPVwiI3NsaWRlNVwiXScpLmFkZENsYXNzKCd2aXNpdGVkJyk7XG5cdFx0XHRcdFx0aWYgKCFhbmNob3JDbGljaykge1xuXHRcdFx0XHRcdFx0JCgnLm5hdi1hY3RpdmUnKS5jc3Moe1xuXHRcdFx0XHRcdFx0XHRsZWZ0OiBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnYW5jaG9yLScgKyAoaW5kZXgrMSkpLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLmxlZnQgKyAkKCcjYW5jaG9yLScgKyAoaW5kZXgrMSkpLndpZHRoKCkvMlxuXHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0fTtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdC8v0L/QtdGA0LXRhdC+0LQg0YEgNiDQvdCwIDUg0YHQu9Cw0LnQtNC10YBcblx0XHRcdFx0ZWxzZSBpZihpbmRleCA9PSA2ICYmIGRpcmVjdGlvbiA9PSd1cCcpe1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uNSAuc2xpZGVfX2hlYWRlcicpLmRlbGF5KDUwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRtYXJnaW5Ub3A6ICc0MDAnLFxuXHRcdFx0XHRcdFx0b3BhY2l0eTogJzAnXG5cdFx0XHRcdFx0fSwgNTApO1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uNSAuYm9yZGVyLWJvdHRvbScpLmRlbGF5KDcwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRtYXJnaW5MZWZ0OiAnMTAwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcwJ1xuXHRcdFx0XHRcdH0sIDUwKTtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjUgLnNsaWRlX19kZXNjcmlwdGlvbicpLmRlbGF5KDEwMDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0bWFyZ2luTGVmdDogJzQwMCcsXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMCdcblx0XHRcdFx0XHR9LCA1MDApO1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uNCAuc2xpZGVfX2hlYWRlcicpLmRlbGF5KDUwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRtYXJnaW5Ub3A6ICcwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcxJ1xuXHRcdFx0XHRcdH0sIDUwMCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb240IC5ib3JkZXItYm90dG9tJykuZGVsYXkoNzAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdG1hcmdpbkxlZnQ6ICcwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcxJ1xuXHRcdFx0XHRcdH0sIDUwMCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb240IC5zbGlkZV9faWNvbnMtZGVzY3JpcHRpb24nKS5lYWNoKGZ1bmN0aW9uKGluZGV4LCB2YWx1ZSkge1xuXHRcdFx0XHRcdFx0dmFyIG1hcmdpbkxlZnQgPSAkKHRoaXMpLnByZXYoKS5vZmZzZXQoKS5sZWZ0IC0gJCgnLmZyYW5jaGlzZS1pY29uX19jaXJjbGUnKS5maXJzdCgpLm9mZnNldCgpLmxlZnQ7XG5cdFx0XHRcdFx0XHQgJCh0aGlzKS5jc3MoJ21hcmdpbi1sZWZ0JywgbWFyZ2luTGVmdCk7XG5cdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0c2V0VGltZW91dChmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdHZhciBzZWxmID0gJCh0aGlzKS5ub3QoJy5zbGlkZV9faWNvbnMtZGVzY3JpcHRpb24nKTtcblx0XHQgICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdCAgICBcdHNlbGYuY3NzKHtcblx0XHRcdCAgICAgICAgICAgICAgICBcdG9wYWNpdHk6ICcxJyxcblx0XHRcdCAgICAgICAgICAgICAgICBcdG1hcmdpblRvcDogJzAnXG5cdFx0ICAgICAgICAgICAgICAgIFx0fSk7XG5cdFx0XHRcdFx0ICAgIH0sIGluZGV4KjUwKTtcblx0XHQgICAgICAgICAgICB9LCA2MDApO1xuXHRcdFx0XHRcdCQoJ1tocmVmPVwiI3NsaWRlNVwiXScpLnJlbW92ZUNsYXNzKCd2aXNpdGVkJylcblx0XHRcdFx0XHRpZiAoIWFuY2hvckNsaWNrKSB7XG5cdFx0XHRcdFx0XHQkKCcubmF2LWFjdGl2ZScpLmNzcyh7XG5cdFx0XHRcdFx0XHRcdGxlZnQ6IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdhbmNob3ItJyArIChpbmRleC0xKSkuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkubGVmdCArICQoJyNhbmNob3ItJyArIChpbmRleC0xKSkud2lkdGgoKS8yXG5cdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHR9O1xuXHRcdFx0XHRcdCQuZm4uZnVsbHBhZ2Uuc2V0QWxsb3dTY3JvbGxpbmcodHJ1ZSwgJ3VwLCBkb3duJyk7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHQvL9C/0LXRgNC10YXQvtC0INGBIDYg0L3QsCA3INGB0LvQsNC50LTQtdGAXG5cdFx0XHRcdGVsc2UgaWYoaW5kZXggPT0gNiAmJiBkaXJlY3Rpb24gPT0nZG93bicpe1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uNSAuc2xpZGVfX2hlYWRlcicpLmRlbGF5KDEwMDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0bWFyZ2luVG9wOiAnNDAwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcwJ1xuXHRcdFx0XHRcdH0sIDUwKTtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjUgLmJvcmRlci1ib3R0b20nKS5kZWxheSgxMDAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdG1hcmdpbkxlZnQ6ICcxMDAnLFxuXHRcdFx0XHRcdFx0b3BhY2l0eTogJzAnXG5cdFx0XHRcdFx0fSwgNTApO1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uNSAuc2xpZGVfX2Rlc2NyaXB0aW9uJykuZGVsYXkoMTAwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRtYXJnaW5MZWZ0OiAnNDAwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcwJ1xuXHRcdFx0XHRcdH0sIDUwKTtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjZfMSAuc2xpZGVfX2hlYWRlcicpLmRlbGF5KDUwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRwYWRkaW5nVG9wOiAnMCcsXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMSdcblx0XHRcdFx0XHR9LCA1MDApO1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uNl8xIC5ib3JkZXItYm90dG9tJykuZGVsYXkoNzAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdHBhZGRpbmdMZWZ0OiAnMCcsXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMSdcblx0XHRcdFx0XHR9LCA1MDApO1xuXHRcdFx0XHRcdCQoJ1tocmVmPVwiI3NsaWRlNlwiXScpLmFkZENsYXNzKCd2aXNpdGVkJyk7XG5cdFx0XHRcdFx0aWYgKCFhbmNob3JDbGljaykge1xuXHRcdFx0XHRcdFx0JCgnLm5hdi1hY3RpdmUnKS5jc3Moe1xuXHRcdFx0XHRcdFx0XHRsZWZ0OiBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnYW5jaG9yLScgKyAoaW5kZXgrMSkpLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLmxlZnQgKyAkKCcjYW5jaG9yLScgKyAoaW5kZXgrMSkpLndpZHRoKCkvMlxuXHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0fTtcblxuXHRcdFx0XHRcdCQuZm4uZnVsbHBhZ2Uuc2V0QWxsb3dTY3JvbGxpbmcoZmFsc2UsICdkb3duJyk7IC8v0LfQv9GA0LXRidCw0LXQvCDRjtC30LXRgNGDINC60YDRg9GC0LjRgtGMINGB0LvQsNC50LTQtdGAINCy0L3QuNC3XG5cdFx0XHRcdH1cblxuXHRcdFx0XHQvL9C/0LXRgNC10YXQvtC0INGBIDcg0L3QsCA2INGB0LvQsNC50LTQtdGAXG5cdFx0XHRcdGVsc2UgaWYoaW5kZXggPT0gNyAmJiBkaXJlY3Rpb24gPT0ndXAnKXtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjZfMSAuc2xpZGVfX2hlYWRlcicpLmRlbGF5KDUwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRwYWRkaW5nVG9wOiAnNDAwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcwJ1xuXHRcdFx0XHRcdH0sIDUwKTtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjZfMSAuYm9yZGVyLWJvdHRvbScpLmRlbGF5KDcwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRwYWRkaW5nTGVmdDogJzQwMCcsXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMCdcblx0XHRcdFx0XHR9LCA1MCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb241IC5zbGlkZV9faGVhZGVyJykuZGVsYXkoNTAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdG1hcmdpblRvcDogJzAnLFxuXHRcdFx0XHRcdFx0b3BhY2l0eTogJzEnXG5cdFx0XHRcdFx0fSwgNTAwKTtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjUgLmJvcmRlci1ib3R0b20nKS5kZWxheSg3MDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0bWFyZ2luTGVmdDogJzAnLFxuXHRcdFx0XHRcdFx0b3BhY2l0eTogJzEnXG5cdFx0XHRcdFx0fSwgNTAwKTtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjUgLnNsaWRlX19kZXNjcmlwdGlvbicpLmRlbGF5KDEwMDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0bWFyZ2luTGVmdDogJzAnLFxuXHRcdFx0XHRcdFx0b3BhY2l0eTogJzEnXG5cdFx0XHRcdFx0fSwgNTAwKTtcblx0XHRcdFx0XHQkKCdbaHJlZj1cIiNzbGlkZTVcIl0nKS5yZW1vdmVDbGFzcygndmlzaXRlZCcpXG5cdFx0XHRcdFx0aWYgKCFhbmNob3JDbGljaykge1xuXHRcdFx0XHRcdFx0JCgnLm5hdi1hY3RpdmUnKS5jc3Moe1xuXHRcdFx0XHRcdFx0XHRsZWZ0OiBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnYW5jaG9yLScgKyAoaW5kZXgtMSkpLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLmxlZnQgKyAkKCcjYW5jaG9yLScgKyAoaW5kZXgtMSkpLndpZHRoKCkvMlxuXHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0fTtcblx0XHRcdFx0XHQkLmZuLmZ1bGxwYWdlLnNldEFsbG93U2Nyb2xsaW5nKHRydWUsICd1cCwgZG93bicpO1xuXHRcdFx0XHR9XG5cblx0XHRcdFx0Ly/Qv9C10YDQtdGF0L7QtCDRgSA4INC90LAgNyDRgdC70LDQudC00LXRgFxuXHRcdFx0XHRlbHNlIGlmKGluZGV4ID09IDggJiYgZGlyZWN0aW9uID09J3VwJyl7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb243IC5zbGlkZV9faGVhZGVyJykuZGVsYXkoMTAwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMCdcblx0XHRcdFx0XHR9LCA1MCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb243IC5zbGlkZV9faGVhZGVyIGgxJykuZGVsYXkoMTAwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRtYXJnaW5MZWZ0OiAnNDAwJ1xuXHRcdFx0XHRcdH0sIDUwKTtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjcgLmJvcmRlci1ib3R0b20nKS5kZWxheSgxMDAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdG1hcmdpbkxlZnQ6ICc0MDAnLFxuXHRcdFx0XHRcdFx0b3BhY2l0eTogJzAnXG5cdFx0XHRcdFx0fSwgNTApO1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uNyAuc2xpZGVfX2hlYWRlcl9fbGlzdCBsaScpLmRlbGF5KDEwMDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0bWFyZ2luTGVmdDogJzQwMCcsXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMCdcblx0XHRcdFx0XHR9LCA1MCk7XG5cdFx0XHRcdFx0JCgnI3NlY3Rpb243IC5zdWJzY3JpYmUtZm9ybScpLmRlbGF5KDEwMDApLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0dG9wOiAnNDAwJyxcblx0XHRcdFx0XHRcdG9wYWNpdHk6ICcwJ1xuXHRcdFx0XHRcdH0sIDUwKTtcblx0XHRcdFx0XHQkKCcjc2VjdGlvbjZfMSAuc2xpZGVfX2hlYWRlcicpLmRlbGF5KDUwMCkuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRwYWRkaW5nVG9wOiAnMCcsXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMSdcblx0XHRcdFx0XHR9LCA1MDApO1xuXHRcdFx0XHRcdCQoJyNzZWN0aW9uNl8xIC5ib3JkZXItYm90dG9tJykuZGVsYXkoNzAwKS5hbmltYXRlKHtcblx0XHRcdFx0XHRcdHBhZGRpbmdMZWZ0OiAnMCcsXG5cdFx0XHRcdFx0XHRvcGFjaXR5OiAnMSdcblx0XHRcdFx0XHR9LCA1MDApO1xuXHRcdFx0XHRcdCQoJ1tocmVmPVwiI3NsaWRlNlwiXScpLmFkZENsYXNzKCd2aXNpdGVkJyk7XG5cdFx0XHRcdFx0aWYgKCFhbmNob3JDbGljaykge1xuXHRcdFx0XHRcdFx0JCgnLm5hdi1hY3RpdmUnKS5jc3Moe1xuXHRcdFx0XHRcdFx0XHRsZWZ0OiBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnYW5jaG9yLScgKyAoaW5kZXgtMSkpLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLmxlZnQgKyAkKCcjYW5jaG9yLScgKyAoaW5kZXgtMSkpLndpZHRoKCkvMlxuXHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0fTtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdGFuY2hvckNsaWNrID0gZmFsc2U7XG5cdFx0XHR9XG5cdFx0fSk7XG5cblx0XHRkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInNlY3Rpb242XCIpLmFkZEV2ZW50TGlzdGVuZXIoJ3doZWVsJywgZnVuY3Rpb24oZSkgeyAvL9C+0YLRgdC70LXQttC40LLQsNC10Lwg0LrQvtC70LXRgdC+INC80YvRiNC4INC90LAg0LPQvtGA0LjQt9C+0L3RgtCw0LvRjNC90L7QvCDRgdC70LDQudC00LXRgNC1XG5cdCAgICAgICAgaWYgKGUuZGVsdGFZIDwgMCkgeyAvL9C/0YDQvtC60YPRgNGC0LrQsCDQstCy0LXRgNGFXG5cdCAgICAgICAgXHRkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInNlY3Rpb242XzFcIikuYWRkRXZlbnRMaXN0ZW5lcignd2hlZWwnLCBmdW5jdGlvbihlKSB7XG5cdFx0XHRcdFx0aWYgKGUuZGVsdGFZIDwgMCkge1xuXHRcdFx0XHRcdFx0JC5mbi5mdWxscGFnZS5tb3ZlVG8oJ3NsaWRlNScpO1xuXHRcdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSk7XG5cdFx0XHRcdCQuZm4uZnVsbHBhZ2UubW92ZVNsaWRlTGVmdCgpO1xuXHQgICAgICAgIH1cblx0ICAgICAgICBpZiAoZS5kZWx0YVkgPiAwKSB7IC8v0L/RgNC+0LrRg9GA0YLQutCwINCy0L3QuNC3XG5cdCAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwic2VjdGlvbjZfNFwiKS5hZGRFdmVudExpc3RlbmVyKCd3aGVlbCcsIGZ1bmN0aW9uKGUpIHtcblx0XHRcdFx0XHRpZiAoZS5kZWx0YVkgPiAwKSB7XG5cdFx0XHRcdFx0XHQkLmZuLmZ1bGxwYWdlLm1vdmVUbygnc2xpZGU3Jyk7XG5cdFx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9KTtcblx0XHRcdFx0JC5mbi5mdWxscGFnZS5tb3ZlU2xpZGVSaWdodCgpO1xuXHQgICAgICAgIH1cblx0XHR9KTtcblxuXHRcdCQoJyNtZW51IGEnKS5jbGljayhmdW5jdGlvbihldmVudCkge1xuXHRcdFx0dmFyIGlkID0gJCh0aGlzKS5hdHRyKCdpZCcpO1xuXHRcdFx0YW5jaG9yQ2xpY2sgPSB0cnVlO1xuXHRcdFx0JCgnLm5hdi1hY3RpdmUnKS5jc3Moe1xuXHRcdFx0XHRsZWZ0OiBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChpZCkuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkubGVmdCArICQoJyMnK2lkKS53aWR0aCgpLzJcblx0XHRcdH0pO1xuXHRcdH0pO1xuXHR9XG5cblx0Ly/Qt9Cw0LTQsNC10Lwg0LDRgtGA0LjQsdGD0YLRiyDRjdC70LXQvNC10L3RgtC+0LIg0YHRgtGA0LDQvdC40YbRiyDQtNC70Y8g0LDQtNCw0L/RgtC40LLQsFxuXHRpZiAocmVzcG9uc2l2ZU1vZGVUZWJsZXQpIHtcblx0XHQkKCcubS1tZW51LWxpc3QgYScpLnJlbW92ZUNsYXNzKCd2aXNpdGVkJyk7XG5cdFx0JCgnLmZyYW5jaGlzZS1pY29uX19jaXJjbGUuYWN0aXZlJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuXHRcdCQoJy5mcmFuY2hpc2UtaWNvbl9fY2lyY2xlLmluYWN0aXZlJykucmVtb3ZlQ2xhc3MoJ2luYWN0aXZlJyk7XG5cdH1cblx0ZWxzZSBpZiAoIXJlc3BvbnNpdmVNb2RlVGVibGV0KXtcblx0XHQkKCcubS1tZW51JykuY3NzKCdkaXNwbGF5JywgJ25vbmUnKTtcblx0fVxuXG5cdC8v0L7QsdGA0LDQsdC+0YLQutCwINC60LvQuNC60L7QsiDQv9C+INC40LrQvtC90LrQsNC8INGB0YLRgNCw0L3QuNGG0YsgXCLRhNGA0LDQvdGI0LjQt9GLXCJcblx0JCgnLmZyYW5jaGlzZS1pY29uX19jaXJjbGUnKS5jbGljayhmdW5jdGlvbigpIHtcblx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpXG5cdFx0aWYgKCQodGhpcykuaGFzQ2xhc3MoJ2luYWN0aXZlJykpIHtcblx0XHRcdCQoJy5mcmFuY2hpc2UtaWNvbl9fY2lyY2xlLmFjdGl2ZScpLmFkZENsYXNzKCdpbmFjdGl2ZScpO1xuXHRcdFx0JCgnLmZyYW5jaGlzZS1pY29uX19jaXJjbGUuYWN0aXZlJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuXHRcdFx0JCh0aGlzKS5hZGRDbGFzcygnYWN0aXZlJyk7XG5cdFx0XHQkKHRoaXMpLnJlbW92ZUNsYXNzKCdpbmFjdGl2ZScpO1xuXG5cblx0XHRcdCQoXCIuc2xpZGVfX2ljb25zLWRlc2NyaXB0aW9uLmFjdGl2ZVwiKVxuXHRcdFx0XHQuYWRkQ2xhc3MoXCJpbmFjdGl2ZVwiKVxuXHRcdFx0XHQucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpXG5cblx0XHRcdGlmICgkKHRoaXMpLm5leHQoKS5oYXNDbGFzcygnaW5hY3RpdmUnKSkge1xuXHRcdFx0XHQkKHRoaXMpLm5leHQoKVxuXHRcdFx0XHRcdC5yZW1vdmVDbGFzcygnaW5hY3RpdmUnKVxuXHRcdFx0XHRcdC5hZGRDbGFzcygnYWN0aXZlJyk7XG5cdFx0XHR9XG5cdFx0fVxuXHR9KTtcblxuXHQvL9C+0LHRgNCw0LHQvtGC0LrQsCDQutC70LjQutC+0LIg0LzQtdC90Y4g0LIg0LDQtNCw0L/RgtC40LLQtVxuXHQkKCcubS1tZW51LWJ1dHRvbicpLmNsaWNrKGZ1bmN0aW9uKCkge1xuXHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KClcblx0XHQkKHRoaXMpLnRvZ2dsZUNsYXNzKCdidXJnZXInKTtcblx0XHQkKHRoaXMpLnRvZ2dsZUNsYXNzKCdjbG9zZScpO1xuXHRcdCQoJy5tLW1lbnUnKS5zbGlkZVRvZ2dsZSgnZmFzdCcsIGZ1bmN0aW9uKCkge30pO1xuXHR9KTtcblxuXHQkKCcubS1tZW51LWxpc3QgYScpLmNsaWNrKGZ1bmN0aW9uKCkge1xuXHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KClcblx0XHQkKCcubS1tZW51LWxpc3QgYScpLnJlbW92ZUNsYXNzKCd2aXNpdGVkJyk7XG5cdFx0JCgnLm0tbWVudS1idXR0b24nKS50b2dnbGVDbGFzcygnYnVyZ2VyJyk7XG5cdFx0JCgnLm0tbWVudS1idXR0b24nKS50b2dnbGVDbGFzcygnY2xvc2UnKTtcblx0XHQkKHRoaXMpLmFkZENsYXNzKCd2aXNpdGVkJyk7XG5cdFx0JCgnLm0tbWVudScpLnNsaWRlVG9nZ2xlKCdmYXN0JywgZnVuY3Rpb24oKSB7fSk7XG5cdFx0JCgnaHRtbCwgYm9keScpLmFuaW1hdGUoe1xuICAgICAgICBcdHNjcm9sbFRvcDogJCgkKHRoaXMpLmF0dHIoJ2hyZWYnKSkub2Zmc2V0KCkudG9wIC0gJChcImhlYWRlclwiKS5oZWlnaHQoKVxuICAgIFx0fSwgMTAwMCk7XG5cdH0pO1xufSk7XG4iXX0=
