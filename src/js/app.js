$(document).ready(function(){
	var responsiveModeTeblet = false;
	var responsiveModeFullHD = false;
	var anchorClick = false;
	var SLIDES_COUNT = 8;

	Modernizr.on('webp', function(result) {
	  if (result) {
	    $('.section').addClass('webp');
	  } else {
	    console.log('WebP is not supported!');
	  }
	});

	if (window.matchMedia('(max-width: 768px)').matches) {
		responsiveModeTeblet = true;
	}

	if (window.matchMedia('(min-width: 1920px)').matches) {
		responsiveModeFullHD = true;
	}

	window.onresize = function() {
		if (window.matchMedia('(max-width: 768px)').matches) {
			responsiveModeTeblet = true;
		}

		else if (window.matchMedia('(min-width: 768px)').matches) {
			responsiveModeTeblet = false;
		}

		if (window.matchMedia('(min-width: 1920px)').matches) {
			responsiveModeFullHD = true;
		}

		else if (window.matchMedia('(max-width: 1920px)').matches) {
			responsiveModeFullHD = false;
		}
	}

	$('.subscribe-form__agree').click(function(event) {
		event.preventDefault()
		if ($('.subscribe-form__agree-checkbox').attr('checked')) {
	        $('.subscribe-form__agree-checkbox').removeAttr('checked')
	    } else {
	        $('.subscribe-form__agree-checkbox').attr('checked', 'checked');
	    }
	});

	function setAnchorActive(index) {
		for (i = SLIDES_COUNT; i>=index; i--) {
			$('[href="#slide' + i + '"]').removeClass('visited');
		}
		for (i = 1; i<index; i++) {
			$('[href="#slide' + i + '"]').addClass('visited');
		}
	}

	function animateListElements(element, delay, speed, margin, interval, direction) {
		element.children().delay(delay).each(function(index, value){
			if (direction == 'left') {
				$(this).delay(interval * index).animate({
                	opacity: '1',
                	marginLeft: margin
            	}, speed, 'swing');
			}
			else if (direction == 'right') {
				$(this).delay(interval * index).animate({
                	opacity: '0',
                	marginLeft: margin
            	}, speed, 'swing');
			}
        });
	}

	function setNavSliderPosition() {
		$('.section').each(function(index, value){
			if ($(this).hasClass('active')) {
				var i = 'anchor-'+(index+1);
				$('.nav-active').css({
					left: document.getElementById(i).getBoundingClientRect().left + $('#'+i).width()/2
				});
			}
		});
	}

	if (!responsiveModeTeblet) {
		$('#fullpage').fullpage({ //инициализируем fullpage.js
			scrollingSpeed: 1000,
			menu: '#menu',
			autoScrolling: true,
			lazyLoading: false,
			anchors: ['slide0', 'slide1', 'slide2', 'slide3', 'slide4', 'slide5', 'slide6', 'slide7'],
			easingcss3: 'ease-in-out',

			afterLoad: function(anchorLink, index){
				var topMargin='100';
				if (responsiveModeFullHD) {
					topMargin='25%';
				}
				else {
					topMargin='100';
				}

				//открытие слайда 1
				if(index == 1){
					$('[href="#slide0"]').addClass('visited');
					for (i = SLIDES_COUNT; i>=index; i--) {
						$('[href="#slide' + i + '"]').removeClass('visited');
					}
					$('#section0 .slide__header').animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					$('#section0 .subscribe-form').delay(200).animate({
						top: topMargin,
						opacity: '1'
					}, 500);
					$('#section0 .border-bottom').delay(300).animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					setNavSliderPosition()
				}

				if(index == 2){
					$('#section1 .slide__header').delay(500).animate({
						marginTop: '0',
						opacity: '1'
					}, 500);
					$('#section1 .border-bottom').delay(700).animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					$('#section1 .video-wrapper').delay(500).animate({
						top: '11%',
						opacity: '1'
					}, 500);
					$('[href="#slide1"]').addClass('visited');
					setNavSliderPosition()
					setAnchorActive(index);
				}

				if(index == 3){
					$('#section2 .slide__header').delay(500).animate({
						marginTop: '0',
						opacity: '1'
					}, 500);
					$('#section2 .border-bottom').delay(700).animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					$('#section2 .slide__img-right').delay(700).animate({
						marginTop: '0',
						opacity: '1'
					}, 500);
					animateListElements($('#section2 .slide__header__list'), 800, 500, 0, 400, 'left');
					$('[href="#slide2"]').addClass('visited');
					setNavSliderPosition()
					setAnchorActive(index);
				}

				if(index == 4){
					$('#section3 .slide__header').delay(500).animate({
						marginTop: '0',
						opacity: '1'
					}, 500);
					$('#section3 .border-bottom').delay(700).animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					$('#section3 .slide__img-right').delay(700).animate({
						marginTop: '0',
						opacity: '1'
					}, 500);
					animateListElements($('#section3 .slide__header__list'), 800, 500, 0, 400, 'left');
					$('[href="#slide3"]').addClass('visited');
					setNavSliderPosition()
					setAnchorActive(index);
				}

				if(index == 5){
					$('#section4 .slide__header').delay(500).animate({
						marginTop: '0',
						opacity: '1'
					}, 500);
					$('#section4 .border-bottom').delay(700).animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					$('.slide__icons-wrapper').children().delay(700).each(function(index, value){
						var self = $(this).not('.slide__icons-description');
		                setTimeout(function () {
						    	self.css({
			                	opacity: '1',
			                	marginTop: '0'
		                	});
					    }, index*50);
		     		});
					$('[href="#slide4"]').addClass('visited');
					$('#section4 .slide__icons-description').each(function(index, value) {
						var marginLeft = $(this).prev().offset().left - $('.franchise-icon__circle').first().offset().left;
						 $(this).css('margin-left', marginLeft);
					});
					setNavSliderPosition()
					setAnchorActive(index);
				}

				if(index == 6){
					$('#section5 .slide__header').delay(500).animate({
						marginTop: '0',
						opacity: '1'
					}, 500);
					$('#section5 .border-bottom').delay(700).animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					$('#section5 .slide__description').delay(1000).animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					$('[href="#slide5"]').addClass('visited');
					setNavSliderPosition()
					setAnchorActive(index);
				}

				if(index == 7){
					$('#section6_1 .slide__header').delay(500).animate({
						paddingTop: '0',
						opacity: '1'
					}, 500);
					$('#section6_1 .border-bottom').delay(700).animate({
						paddingLeft: '0',
						opacity: '1'
					}, 500);
					$('[href="#slide6"]').addClass('visited');
					setNavSliderPosition()
					setAnchorActive(index);
				}

				if(index == 8){
					$('#section7 .slide__header h1').animate({
						marginLeft: '0'
					}, 500);
					$('#section7 .slide__header').animate({
						opacity: '1'
					}, 500);
					$('#section7 .subscribe-form').delay(200).animate({
						top: topMargin,
						opacity: '1'
					}, 500);
					$('#section7 .border-bottom').delay(300).animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					animateListElements($('#section7 .slide__header__list'), 800, 500, 0, 400, 'left');
					$('[href="#slide7"]').addClass('visited');
					setNavSliderPosition()
					setAnchorActive(index);
				}

				anchorClick = false;
			},

			onLeave: function(index, nextIndex, direction){
				//переход с 1 на 2 слайдер
				if(index == 1 && direction =='down'){
					$('#section0 .slide__header').delay(1000).animate({
						marginLeft: '400',
						opacity: '0'
					}, 50);
					$('#section0 .subscribe-form').delay(1200).animate({
						opacity: '0'}).delay(100).animate({top: '400px'});
					$('#section0 .border-bottom').delay(1300).animate({
						marginLeft: '100',
						opacity: '0'
					}, 50);
					$('#section1 .slide__header').delay(500).animate({
						marginTop: '0',
						opacity: '1'
					}, 500);
					$('#section1 .border-bottom').delay(700).animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					$('#section1 .video-wrapper').show('fast', function(){}).delay(500).animate({
						top: '11%',
						opacity: '1'
					}, 300);
					$('[href="#slide1"]').addClass('visited');
					if (!anchorClick) {
						$('.nav-active').css({
							left: document.getElementById('anchor-' + (index+1)).getBoundingClientRect().left + $('#anchor-' + (index+1)).width()/2
						});
					};
				}

				//переход со 2 на 1 слайдер
				else if(index == 2 && direction =='up'){
					$('#section1 .slide__header').delay(1000).animate({
						marginTop: '400',
						opacity: '0'
					}, 50);
					$('#section1 .border-bottom').delay(1000).animate({
						marginLeft: '100',
						opacity: '0'
					}, 50);
					$('#section1 .video-wrapper').delay(1000).animate({
						top: '90%',
						opacity: '0'
					}, 50);
					$('[href="#slide1"]').removeClass('visited')
					if (!anchorClick) {
						$('.nav-active').css({
							left: document.getElementById('anchor-' + (index-1)).getBoundingClientRect().left + $('#anchor-' + (index-1)).width()/2
						});
					};
				}

				//переход со 2 на 3 слайдер
				else if(index == 2 && direction =='down'){
					$('#section1 .slide__header').delay(1000).animate({
						marginTop: '400',
						opacity: '0'
					}, 50);
					$('#section1 .border-bottom').delay(1000).animate({
						marginLeft: '100',
						opacity: '0'
					}, 50);
					$('#section1 .video-wrapper').delay(1000).hide('fast', function() {}).animate({
						top: '90%',
						opacity: '0'
					});
					$('#section2 .slide__header').delay(500).animate({
						marginTop: '0',
						opacity: '1'
					}, 50);
					$('#section2 .border-bottom').delay(700).animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					$('#section2 .slide__img-right').delay(700).animate({
						marginTop: '0',
						opacity: '1'
					}, 500);
					animateListElements($('#section2 .slide__header__list'), 800, 500, 0, 400, 'left');
					$('[href="#slide2"]').addClass('visited');
					if (!anchorClick) {
						$('.nav-active').css({
							left: document.getElementById('anchor-' + (index+1)).getBoundingClientRect().left + $('#anchor-' + (index+1)).width()/2
						});
					};
				}

				//переход с 3 на 2 слайдер
				else if(index == 3 && direction =='up'){
					$('#section1 .slide__header').delay(500).animate({
						marginTop: '0',
						opacity: '1'
					}, 500);
					$('#section1 .border-bottom').delay(700).animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					$('#section1 .video-wrapper').delay(500).show('fast', function(){}).animate({
						top: '11%',
						opacity: '1'
					}, 300);
					$('#section2 .slide__header').delay(500).animate({
						marginTop: '400',
						opacity: '0'
					}, 50);
					$('#section2 .border-bottom').delay(700).animate({
						marginLeft: '100',
						opacity: '0'
					}, 50);
					$('#section2 .slide__img-right').delay(700).animate({
						marginTop: '400',
						opacity: '0'
					}, 50);
					animateListElements($('#section2 .slide__header__list'), 950, 10, 300, 0, 'right');
					$('[href="#slide2"]').removeClass('visited')
					if (!anchorClick) {
						$('.nav-active').css({
							left: document.getElementById('anchor-' + (index-1)).getBoundingClientRect().left + $('#anchor-' + (index-1)).width()/2
						});
					};
				}

				//переход с 3 на 4 слайдер
				else if(index == 3 && direction =='down'){
					$('#section2 .slide__header').delay(500).animate({
						marginTop: '400',
						opacity: '0'
					}, 50);
					$('#section2 .border-bottom').delay(700).animate({
						marginLeft: '100',
						opacity: '0'
					}, 50);
					$('#section2 .slide__img-right').delay(700).animate({
						marginTop: '400px',
						opacity: '0'
					}, 50);
					$('#section2 .slide__header__list li').delay(1000).animate({
						marginLeft: '300',
						opacity: '0'
					}, 10);
					$('#section3 .slide__header').delay(500).animate({
						marginTop: '0',
						opacity: '1'
					}, 500);
					$('#section3 .border-bottom').delay(700).animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					$('#section3 .slide__img-right').delay(700).animate({
						marginTop: '0',
						opacity: '1'
					}, 500);
					animateListElements($('#section3 .slide__header__list'), 800, 500, 0, 400, 'left');
					$('[href="#slide3"]').addClass('visited');
					if (!anchorClick) {
						$('.nav-active').css({
							left: document.getElementById('anchor-' + (index+1)).getBoundingClientRect().left + $('#anchor-' + (index+1)).width()/2
						});
					};
				}

				//переход с 4 на 3 слайдер
				else if(index == 4 && direction =='up'){
					$('#section3 .slide__header').delay(500).animate({
						marginTop: '400',
						opacity: '0'
					}, 50);
					$('#section3 .border-bottom').delay(700).animate({
						marginLeft: '100',
						opacity: '0'
					}, 50);
					$('#section3 .slide__img-right').delay(700).animate({
						marginTop: '400',
						opacity: '0'
					}, 50);
					animateListElements($('#section3 .slide__header__list'), 950, 10, 300, 0, 'right');
					$('#section2 .slide__header').delay(500).animate({
						marginTop: '0',
						opacity: '1'
					}, 500);
					$('#section2 .border-bottom').delay(700).animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					$('#section2 .slide__img-right').delay(700).animate({
						marginTop: '0',
						opacity: '1'
					}, 500);
					animateListElements($('#section2 .slide__header__list'), 800, 500, 0, 400, 'left');
					$('[href="#slide3"]').removeClass('visited')
					if (!anchorClick) {
						$('.nav-active').css({
							left: document.getElementById('anchor-' + (index-1)).getBoundingClientRect().left + $('#anchor-' + (index-1)).width()/2
						});
					};
				}

				//переход с 4 на 5 слайдер
				else if(index == 4 && direction =='down'){
					$('#section3 .slide__header').delay(500).animate({
						marginTop: '400',
						opacity: '0'
					}, 50);
					$('#section3 .border-bottom').delay(700).animate({
						marginLeft: '100',
						opacity: '0'
					}, 50);
					$('#section3 .slide__img-right').delay(700).animate({
						marginTop: '120',
						opacity: '0'
					}, 50);
					$('#section3 .slide__header__list li').delay(500).animate({
						marginLeft: '320',
						opacity: '0'
					}, 500);
					$('#section4 .slide__header').delay(500).animate({
						marginTop: '0',
						opacity: '1'
					}, 500);
					$('#section4 .border-bottom').delay(700).animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					$('#section4 .slide__icons-description').each(function(index, value) {
						var marginLeft = $(this).prev().offset().left - $('.franchise-icon__circle').first().offset().left;
						 $(this).css('margin-left', marginLeft);
					});
					$('.slide__icons-wrapper').children().each(function(index, value){
					setTimeout(function() {
						var self = $(this).not('.slide__icons-description');
		                setTimeout(function() {
						    	self.css({
			                	opacity: '1',
			                	marginTop: '0'
		                	});
					    }, index*50);
		            }, 600);
		      });
					$('[href="#slide4"]').addClass('visited');
					if (!anchorClick) {
						$('.nav-active').css({
							left: document.getElementById('anchor-' + (index+1)).getBoundingClientRect().left + $('#anchor-' + (index+1)).width()/2
						});
					};
				}

				//переход с 5 на 4 слайдер
				else if(index == 5 && direction =='up'){
					$('#section4 .slide__header').delay(500).animate({
						marginTop: '400',
						opacity: '0'
					}, 50);
					$('#section4 .border-bottom').delay(700).animate({
						marginLeft: '100',
						opacity: '0'
					}, 50);
					$('#section4 .franchise-icon__circle').delay(1000).animate({
						marginTop: '300',
						opacity: '0'
					}, 50);
					$('#section4 .franchise-icon-dot').delay(1000).animate({
						marginTop: '300',
						opacity: '0'
					}, 50);
					$('#section4 .slide__icons-description').delay(700).animate({
						marginLeft: '400',
						opacity: '0'
					}, 50);
					$('#section3 .slide__header').delay(500).animate({
						marginTop: '0',
						opacity: '1'
					}, 500);
					$('#section3 .border-bottom').delay(700).animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					animateListElements($('#section3 .slide__header__list'), 800, 500, 0, 400, 'left');
					$('#section3 .slide__img-right').delay(700).animate({
						marginTop: '0',
						opacity: '1'
					}, 500);

					$('[href="#slide4"]').removeClass('visited')
					if (!anchorClick) {
						$('.nav-active').css({
							left: document.getElementById('anchor-' + (index-1)).getBoundingClientRect().left + $('#anchor-' + (index-1)).width()/2
						});
					};
				}

				//переход с 5 на 6 слайдер
				else if(index == 5 && direction =='down'){
					$('#section4 .slide__header').delay(1000).animate({
						marginTop: '400',
						opacity: '0'
					}, 50);
					$('#section4 .border-bottom').delay(1000).animate({
						marginLeft: '100',
						opacity: '0'
					}, 50);
					$('#section4 .franchise-icon__circle').delay(1000).animate({
						marginTop: '300',
						opacity: '0'
					}, 50);
					$('#section4 .franchise-icon-dot').delay(1000).animate({
						marginTop: '300',
						opacity: '0'
					}, 50);
					$('#section4 .slide__icons-description').delay(700).animate({
						marginLeft: '400',
						opacity: '0'
					}, 50);
					$('#section5 .slide__header').delay(500).animate({
						marginTop: '0',
						opacity: '1'
					}, 500);
					$('#section5 .border-bottom').delay(700).animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					$('#section5 .slide__description').delay(1000).animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					$('[href="#slide5"]').addClass('visited');
					if (!anchorClick) {
						$('.nav-active').css({
							left: document.getElementById('anchor-' + (index+1)).getBoundingClientRect().left + $('#anchor-' + (index+1)).width()/2
						});
					};
				}

				//переход с 6 на 5 слайдер
				else if(index == 6 && direction =='up'){
					$('#section5 .slide__header').delay(500).animate({
						marginTop: '400',
						opacity: '0'
					}, 50);
					$('#section5 .border-bottom').delay(700).animate({
						marginLeft: '100',
						opacity: '0'
					}, 50);
					$('#section5 .slide__description').delay(1000).animate({
						marginLeft: '400',
						opacity: '0'
					}, 500);
					$('#section4 .slide__header').delay(500).animate({
						marginTop: '0',
						opacity: '1'
					}, 500);
					$('#section4 .border-bottom').delay(700).animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					$('#section4 .slide__icons-description').each(function(index, value) {
						var marginLeft = $(this).prev().offset().left - $('.franchise-icon__circle').first().offset().left;
						 $(this).css('margin-left', marginLeft);
					});
					setTimeout(function() {
						var self = $(this).not('.slide__icons-description');
		                setTimeout(function() {
						    	self.css({
			                	opacity: '1',
			                	marginTop: '0'
		                	});
					    }, index*50);
		            }, 600);
					$('[href="#slide5"]').removeClass('visited')
					if (!anchorClick) {
						$('.nav-active').css({
							left: document.getElementById('anchor-' + (index-1)).getBoundingClientRect().left + $('#anchor-' + (index-1)).width()/2
						});
					};
					$.fn.fullpage.setAllowScrolling(true, 'up, down');
				}

				//переход с 6 на 7 слайдер
				else if(index == 6 && direction =='down'){
					$('#section5 .slide__header').delay(1000).animate({
						marginTop: '400',
						opacity: '0'
					}, 50);
					$('#section5 .border-bottom').delay(1000).animate({
						marginLeft: '100',
						opacity: '0'
					}, 50);
					$('#section5 .slide__description').delay(1000).animate({
						marginLeft: '400',
						opacity: '0'
					}, 50);
					$('#section6_1 .slide__header').delay(500).animate({
						paddingTop: '0',
						opacity: '1'
					}, 500);
					$('#section6_1 .border-bottom').delay(700).animate({
						paddingLeft: '0',
						opacity: '1'
					}, 500);
					$('[href="#slide6"]').addClass('visited');
					if (!anchorClick) {
						$('.nav-active').css({
							left: document.getElementById('anchor-' + (index+1)).getBoundingClientRect().left + $('#anchor-' + (index+1)).width()/2
						});
					};

					$.fn.fullpage.setAllowScrolling(false, 'down'); //зпрещаем юзеру крутить слайдер вниз
				}

				//переход с 7 на 6 слайдер
				else if(index == 7 && direction =='up'){
					$('#section6_1 .slide__header').delay(500).animate({
						paddingTop: '400',
						opacity: '0'
					}, 50);
					$('#section6_1 .border-bottom').delay(700).animate({
						paddingLeft: '400',
						opacity: '0'
					}, 50);
					$('#section5 .slide__header').delay(500).animate({
						marginTop: '0',
						opacity: '1'
					}, 500);
					$('#section5 .border-bottom').delay(700).animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					$('#section5 .slide__description').delay(1000).animate({
						marginLeft: '0',
						opacity: '1'
					}, 500);
					$('[href="#slide5"]').removeClass('visited')
					if (!anchorClick) {
						$('.nav-active').css({
							left: document.getElementById('anchor-' + (index-1)).getBoundingClientRect().left + $('#anchor-' + (index-1)).width()/2
						});
					};
					$.fn.fullpage.setAllowScrolling(true, 'up, down');
				}

				//переход с 8 на 7 слайдер
				else if(index == 8 && direction =='up'){
					$('#section7 .slide__header').delay(1000).animate({
						opacity: '0'
					}, 50);
					$('#section7 .slide__header h1').delay(1000).animate({
						marginLeft: '400'
					}, 50);
					$('#section7 .border-bottom').delay(1000).animate({
						marginLeft: '400',
						opacity: '0'
					}, 50);
					$('#section7 .slide__header__list li').delay(1000).animate({
						marginLeft: '400',
						opacity: '0'
					}, 50);
					$('#section7 .subscribe-form').delay(1000).animate({
						top: '400',
						opacity: '0'
					}, 50);
					$('#section6_1 .slide__header').delay(500).animate({
						paddingTop: '0',
						opacity: '1'
					}, 500);
					$('#section6_1 .border-bottom').delay(700).animate({
						paddingLeft: '0',
						opacity: '1'
					}, 500);
					$('[href="#slide6"]').addClass('visited');
					if (!anchorClick) {
						$('.nav-active').css({
							left: document.getElementById('anchor-' + (index-1)).getBoundingClientRect().left + $('#anchor-' + (index-1)).width()/2
						});
					};
				}

				anchorClick = false;
			}
		});

		document.getElementById("section6").addEventListener('wheel', function(e) { //отслеживаем колесо мыши на горизонтальном слайдере
	        if (e.deltaY < 0) { //прокуртка вверх
	        	document.getElementById("section6_1").addEventListener('wheel', function(e) {
					if (e.deltaY < 0) {
						$.fn.fullpage.moveTo('slide5');
						return;
					}
				});
				$.fn.fullpage.moveSlideLeft();
	        }
	        if (e.deltaY > 0) { //прокуртка вниз
	            document.getElementById("section6_4").addEventListener('wheel', function(e) {
					if (e.deltaY > 0) {
						$.fn.fullpage.moveTo('slide7');
						return;
					}
				});
				$.fn.fullpage.moveSlideRight();
	        }
		});

		$('#menu a').click(function(event) {
			var id = $(this).attr('id');
			anchorClick = true;
			$('.nav-active').css({
				left: document.getElementById(id).getBoundingClientRect().left + $('#'+id).width()/2
			});
		});
	}

	//задаем атрибуты элементов страницы для адаптива
	if (responsiveModeTeblet) {
		$('.m-menu-list a').removeClass('visited');
		$('.franchise-icon__circle.active').removeClass('active');
		$('.franchise-icon__circle.inactive').removeClass('inactive');
	}
	else if (!responsiveModeTeblet){
		$('.m-menu').css('display', 'none');
	}

	//обработка кликов по иконкам страницы "франшизы"
	$('.franchise-icon__circle').click(function() {
		event.preventDefault()
		if ($(this).hasClass('inactive')) {
			$('.franchise-icon__circle.active').addClass('inactive');
			$('.franchise-icon__circle.active').removeClass('active');
			$(this).addClass('active');
			$(this).removeClass('inactive');


			$(".slide__icons-description.active")
				.addClass("inactive")
				.removeClass('active')

			if ($(this).next().hasClass('inactive')) {
				$(this).next()
					.removeClass('inactive')
					.addClass('active');
			}
		}
	});

	//обработка кликов меню в адаптиве
	$('.m-menu-button').click(function() {
		event.preventDefault()
		$(this).toggleClass('burger');
		$(this).toggleClass('close');
		$('.m-menu').slideToggle('fast', function() {});
	});

	$('.m-menu-list a').click(function() {
		event.preventDefault()
		$('.m-menu-list a').removeClass('visited');
		$('.m-menu-button').toggleClass('burger');
		$('.m-menu-button').toggleClass('close');
		$(this).addClass('visited');
		$('.m-menu').slideToggle('fast', function() {});
		$('html, body').animate({
        	scrollTop: $($(this).attr('href')).offset().top - $("header").height()
    	}, 1000);
	});
});
